#!/bin/bash

echo "${TXT_S}Publish Conan pkg ...${TXT_CLEAR}"
echo "pwd = "`pwd`

export CONAN_USER_HOME="$PWD/_conan"
echo "CONAN_USER_HOME=$CONAN_USER_HOME"

cd build/conan

CONAN_LOGIN_USERNAME=ci_user CONAN_PASSWORD=${CI_JOB_TOKEN} conan upload ESysFile/0.1.0@libesys+esysfile/test --all --remote=conan_project
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Failed to public Conand pkg to GitLab.${TXT_CLEAR}"
   exit 1
fi

echo "pwd = "`pwd`
echo "${TXT_S}Publish Conan pkg done.${TXT_CLEAR}"
