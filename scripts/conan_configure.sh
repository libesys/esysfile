#!/bin/bash

echo "${TXT_S}Configure Conan ...${TXT_CLEAR}"
echo "pwd = "`pwd`

export CONAN_USER_HOME="$PWD/_conan"
echo "CONAN_USER_HOME=$CONAN_USER_HOME"

mkdir -p build/conan
cd build/conan

conan --version
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Couldn't get conan vesion.${TXT_CLEAR}"
   exit 1
fi

conan remote add libesys_conan https://artifactory.libesys.org/artifactory/api/conan/libesys --force
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Couldn't add LibESys Artifactory conan remote.${TXT_CLEAR}"
   exit 1
fi

# conan remote add gitlab https://gitlab.com/api/v4/packages/conan --force
# if [ ! $? -eq 0 ]; then
#    echo "${TXT_E}Couldn't add GitLab conan remote.${TXT_CLEAR}"
#    exit 1
# fi

# conan remote add conan_project ${CI_API_V4_URL}/projects/$CI_PROJECT_ID/packages/conan --force
# if [ ! $? -eq 0 ]; then
#    echo "${TXT_E}Couldn't add GitLab conan remote.${TXT_CLEAR}"
#    exit 1
# fi

echo "pwd = "`pwd`
echo "${TXT_S}configure Conan done.${TXT_CLEAR}"
