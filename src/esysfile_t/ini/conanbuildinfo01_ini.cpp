/*!
 * \file esysfile_t/ini/conanbuildinfo01_ini.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <esysfile/ini/file.h>

#include <iostream>

namespace esysfile::ini::test::conanbuildinfo01
{

const std::vector<std::string> s_section_names = {
    "includedirs",
    "libdirs",
    "bindirs",
    "resdirs",
    "builddirs",
    "libs",
    "system_libs",
    "defines",
    "cppflags",
    "cxxflags",
    "cflags",
    "sharedlinkflags",
    "exelinkflags",
    "sysroot",
    "frameworks",
    "frameworkdirs",
    "includedirs_xmlsec",
    "libdirs_xmlsec",
    "bindirs_xmlsec",
    "resdirs_xmlsec",
    "builddirs_xmlsec",
    "libs_xmlsec",
    "system_libs_xmlsec",
    "defines_xmlsec",
    "cppflags_xmlsec",
    "cxxflags_xmlsec",
    "cflags_xmlsec",
    "sharedlinkflags_xmlsec",
    "exelinkflags_xmlsec",
    "sysroot_xmlsec",
    "frameworks_xmlsec",
    "frameworkdirs_xmlsec",
    "rootpath_xmlsec",
    "name_xmlsec",
    "version_xmlsec",
    "generatornames_xmlsec",
    "generatorfilenames_xmlsec",
    "includedirs_libxml2",
    "libdirs_libxml2",
    "bindirs_libxml2",
    "resdirs_libxml2",
    "builddirs_libxml2",
    "libs_libxml2",
    "system_libs_libxml2",
    "defines_libxml2",
    "cppflags_libxml2",
    "cxxflags_libxml2",
    "cflags_libxml2",
    "sharedlinkflags_libxml2",
    "exelinkflags_libxml2",
    "sysroot_libxml2",
    "frameworks_libxml2",
    "frameworkdirs_libxml2",
    "rootpath_libxml2",
    "name_libxml2",
    "version_libxml2",
    "generatornames_libxml2",
    "generatorfilenames_libxml2",
    "includedirs_openssl",
    "libdirs_openssl",
    "bindirs_openssl",
    "resdirs_openssl",
    "builddirs_openssl",
    "libs_openssl",
    "system_libs_openssl",
    "defines_openssl",
    "cppflags_openssl",
    "cxxflags_openssl",
    "cflags_openssl",
    "sharedlinkflags_openssl",
    "exelinkflags_openssl",
    "sysroot_openssl",
    "frameworks_openssl",
    "frameworkdirs_openssl",
    "rootpath_openssl",
    "name_openssl",
    "version_openssl",
    "generatornames_openssl",
    "generatorfilenames_openssl",
    "includedirs_libiconv",
    "libdirs_libiconv",
    "bindirs_libiconv",
    "resdirs_libiconv",
    "builddirs_libiconv",
    "libs_libiconv",
    "system_libs_libiconv",
    "defines_libiconv",
    "cppflags_libiconv",
    "cxxflags_libiconv",
    "cflags_libiconv",
    "sharedlinkflags_libiconv",
    "exelinkflags_libiconv",
    "sysroot_libiconv",
    "frameworks_libiconv",
    "frameworkdirs_libiconv",
    "rootpath_libiconv",
    "name_libiconv",
    "version_libiconv",
    "generatornames_libiconv",
    "generatorfilenames_libiconv",
    "includedirs_zlib",
    "libdirs_zlib",
    "bindirs_zlib",
    "resdirs_zlib",
    "builddirs_zlib",
    "libs_zlib",
    "system_libs_zlib",
    "defines_zlib",
    "cppflags_zlib",
    "cxxflags_zlib",
    "cflags_zlib",
    "sharedlinkflags_zlib",
    "exelinkflags_zlib",
    "sysroot_zlib",
    "frameworks_zlib",
    "frameworkdirs_zlib",
    "rootpath_zlib",
    "name_zlib",
    "version_zlib",
    "generatornames_zlib",
    "generatorfilenames_zlib",
    "USER_libiconv",
    "USER_libxml2",
    "USER_openssl",
    "USER_xmlsec",
    "USER_zlib",
    "ENV_xmlsec",
    "ENV_libxml2",
    "ENV_openssl",
    "ENV_libiconv",
    "ENV_zlib",
};

ESYSTEST_AUTO_TEST_CASE(IniConanBuildInfo01)
{
    boost::filesystem::path path;

    path = esysfile_t::TestCaseCtrl::get().GetTestFilesFolder();
    path /= "conan";
    path /= "conanbuildinfo.txt";

    File ini_read;

    int result = ini_read.read(path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    ESYSTEST_REQUIRE_EQUAL(ini_read.get_sections().size(), s_section_names.size());

    for (auto idx = 0; idx < ini_read.get_sections().size(); ++idx)
    {
        ESYSTEST_REQUIRE_EQUAL(ini_read.get_sections()[idx]->get_name(), s_section_names[idx]);
    }

    auto section = ini_read.find_section("libdirs");
    ESYSTEST_REQUIRE_NE(section, nullptr);
    ESYSTEST_REQUIRE_EQUAL(section->get_elements().size(), 5);
    auto element = section->get_elements()[2];
    ESYSTEST_REQUIRE_NE(element, nullptr);
    ESYSTEST_REQUIRE_EQUAL(
        element->get_name(),
        "C:/Users/miche/.conan/data/openssl/1.1.1k/_/_/package/d057732059ea44a47760900cb5e4855d2bea8714/lib");

    section = ini_read.find_section("generatornames_libxml2");
    ESYSTEST_REQUIRE_NE(section, nullptr);
    ESYSTEST_REQUIRE_EQUAL(section->get_elements().size(), 3);
    element = section->get_elements()[1];
    ESYSTEST_REQUIRE_NE(element, nullptr);
    ESYSTEST_REQUIRE_EQUAL(element->get_name(), "cmake_find_package_multi");

    KeyValue *key_value = dynamic_cast<KeyValue *>(element.get());
    ESYSTEST_REQUIRE_EQUAL(key_value->get_value(), "LibXml2");
}

} // namespace esysfile::ini::test::conanbuildinfo01
