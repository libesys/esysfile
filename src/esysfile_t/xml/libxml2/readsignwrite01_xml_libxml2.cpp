/*!
 * \file esysfile_t/xml/libxml2/readsignwrite01_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <esysfile/xml/data.h>
#include <esysfile/xml/libxml2/file.h>
#include <esysfile/xml/attr.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace libxml2
{

namespace readsignwrite01
{

ESYSTEST_AUTO_TEST_CASE(ReadSignWrite01XMLLibXML2)
{
    boost::filesystem::path file_path = TestCaseCtrl::get().GetTestFilesFolder();
    file_path /= "sdk.esysconf";

    esysfile::xml::libxml2::File xml_read;

    xml_read.set_root_node_name("esyssdk_conf");
    int result = xml_read.read(file_path.string());
    BOOST_REQUIRE_EQUAL(result, 0);

    boost::filesystem::path cert_path = TestCaseCtrl::get().GetTestFilesFolder();
    cert_path /= "esysfile_user001.crt";

    boost::filesystem::path key_path = TestCaseCtrl::get().GetTestFilesFolder();
    key_path /= "esysfile_user001.key";

    esysfile::xml::libxml2::File xml_write;

    xml_write.set_cert_file(cert_path.string());
    xml_write.set_key_file(key_path.string());

    auto data = xml_read.get_data();

    xml_write.set_root_node_name(data->get_root_node_name());
    xml_write.set_data(data);
    xml_write.set_digitally_signed(true);

    boost::filesystem::path out_file_path = TestCaseCtrl::get().GetTempFilesFolder();
    out_file_path /= "sdk.signed.esysconf";

    result = xml_write.write(out_file_path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace readsignwrite01

} // namespace libxml2

} // namespace xml

} // namespace esysfile_t
