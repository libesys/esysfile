/*!
 * \file esysfile_t/xml/libxml2/read01_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <esysfile/xml/data.h>
#include <esysfile/xml/libxml2/file.h>
#include <esysfile/xml/attr.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace libxml2
{

namespace read01
{

void test_data(std::shared_ptr<esysfile::xml::Data> data)
{
    ESYSTEST_REQUIRE_EQUAL(36, data->get_element_count());
    auto pel = data->get_element(0);
    ESYSTEST_REQUIRE_NE(pel, nullptr);
    ESYSTEST_REQUIRE_EQUAL("PLANT", pel->get_name());
    ESYSTEST_REQUIRE_EQUAL(0, pel->get_attr_count());
    ESYSTEST_REQUIRE_EQUAL(6, pel->get_element_count());

    auto pchild = pel->get_element(0);
    ESYSTEST_REQUIRE_NE(pchild, nullptr);
    ESYSTEST_REQUIRE_EQUAL(0, pchild->get_attr_count());
    ESYSTEST_REQUIRE_EQUAL("COMMON", pchild->get_name());
    ESYSTEST_REQUIRE_EQUAL("Bloodroot", pchild->get_value());

    pchild = pel->get_element(1);
    ESYSTEST_REQUIRE_NE(pchild, nullptr);
    ESYSTEST_REQUIRE_EQUAL(0, pchild->get_attr_count());
    ESYSTEST_REQUIRE_EQUAL("BOTANICAL", pchild->get_name());
    ESYSTEST_REQUIRE_EQUAL("Sanguinaria canadensis", pchild->get_value());
}

ESYSTEST_AUTO_TEST_CASE(Read01XMLLibXML2)
{
    boost::filesystem::path path;

    path = TestCaseCtrl::get().GetTestFilesFolder();
    path /= "plant_catalog.xml";

    esysfile::xml::libxml2::File xml_read;

    xml_read.set_root_node_name("CATALOG");
    int result = xml_read.read(path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    test_data(xml_read.get_data());
}

} // namespace read01

} // namespace libxml2

} // namespace xml

} // namespace esysfile_t
