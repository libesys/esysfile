/*!
 * \file esysfile_t/xml/libxml2/writesigned01_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"
#include "esysfile_t/xml/fixture/data02.h"

#include <esysfile/xml/data.h>
#include <esysfile/xml/libxml2/file.h>
#include <esysfile/xml/attr.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace libxml2
{

namespace writesigned01
{

ESYSTEST_AUTO_TEST_CASE(WriteSigned01XMLLibXML2)
{
    boost::filesystem::path out_file_path = TestCaseCtrl::get().GetTempFilesFolder();
    out_file_path /= "writesigned01.xml";

    boost::filesystem::path cert_path = TestCaseCtrl::get().GetTestFilesFolder();
    cert_path /= "esysfile_user001.crt";

    boost::filesystem::path key_path = TestCaseCtrl::get().GetTestFilesFolder();
    key_path /= "esysfile_user001.key";

    xml::fixture::Data02 data02;
    esysfile::xml::libxml2::File xml_write;

    xml_write.set_cert_file(cert_path.string());
    xml_write.set_key_file(key_path.string());

    std::shared_ptr<esysfile::xml::Data> data = std::make_shared<esysfile::xml::Data>();

    data02.create(data);

    xml_write.set_data(data);
    xml_write.set_digitally_signed(true);

    int result = xml_write.write(out_file_path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace writesigned01

} // namespace libxml2

} // namespace xml

} // namespace esysfile_t
