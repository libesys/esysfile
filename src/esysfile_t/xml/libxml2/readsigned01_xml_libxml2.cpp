/*!
 * \file esysfile_t/xml/libxml2/readsigned01_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <esysfile/xml/data.h>
#include <esysfile/xml/libxml2/file.h>
#include <esysfile/xml/attr.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace libxml2
{

namespace readsigned01
{

ESYSTEST_AUTO_TEST_CASE(ReadSigned01XMLLibXML2)
{
    boost::filesystem::path path = TestCaseCtrl::get().GetTestFilesFolder();
    path /= "sdk.signed.esysconf";

    boost::filesystem::path root_cert_file = TestCaseCtrl::get().GetTestFilesFolder();
    root_cert_file /= "esysfile_dev_rootCA.crt";

    esysfile::xml::libxml2::File xml_read;

    xml_read.add_trusted_cert_file(root_cert_file.string());

    xml_read.set_root_node_name("esyssdk_conf");
    int result = xml_read.read(path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);
    ESYSTEST_REQUIRE_EQUAL(xml_read.get_digitally_signed(), true);
    ESYSTEST_REQUIRE_EQUAL(xml_read.get_signature_verification_result(),
                           esysfile::xml::SignatureVerificationResult::VALID_SIGNATURE);
}

} // namespace readsigned01

} // namespace libxml2

} // namespace xml

} // namespace esysfile_t
