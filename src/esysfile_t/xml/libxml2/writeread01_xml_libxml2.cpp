/*!
 * \file esysfile_t/xml/libxml2/writeread01_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/xml/fixture/writereadtest.h"
#include "esysfile_t/xml/fixture/data01.h"

#include <esysfile/xml/libxml2/file.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace libxml2
{

ESYSTEST_AUTO_TEST_CASE(Write01XMLLibXML2)
{
    esysfile::xml::libxml2::File xml_write;
    esysfile::xml::libxml2::File xml_read;
    xml::fixture::Data01 data01;

    fixture::WriteReadTest test("write01xmllibxml2.xml", &xml_write, &xml_read, &data01);

    test.run();
}

} // namespace libxml2

} // namespace xml

} // namespace esysfile_t
