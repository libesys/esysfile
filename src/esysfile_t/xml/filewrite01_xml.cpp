/*!
 * \file esysfile_t/xml/filewrite01_xml.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/xml/fixture/writereadtest.h"
#include "esysfile_t/xml/fixture/data01.h"

#include <esysfile/xml/poco/file.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace xmlfilewrite01
{

ESYSTEST_AUTO_TEST_CASE(XMLFileWrite01)
{
    esysfile::xml::poco::File xml_write;
    esysfile::xml::poco::File xml_read;
    xml::fixture::Data01 data01;

    fixture::WriteReadTest test("xmlfilewrite01.xml", &xml_write, &xml_read, &data01);

    test.run();
}

} // namespace xmlfilewrite01

} // namespace xml

} // namespace esysfile_t
