/*!
 * \file esysfile_t/xml/readcomp01_xml.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <esysfile/xml/data.h>
#include <esysfile/xml/libxml2/file.h>
#include <esysfile/xml/poco/file.h>
#include <esysfile/xml/attr.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace readcomp01
{

void read_comp_file(const std::string &file, const std::string &root_node_name)
{
    esysfile::xml::libxml2::File xml_read_libxml2;

    std::string root_name = root_node_name;
    std::string root_prefix;

    auto i = root_node_name.find(":");

    if (i != std::string::npos)
    {
        root_prefix = root_node_name.substr(0, i);
        root_name = root_node_name.substr(i + 1);
    }

    xml_read_libxml2.set_root_node_name(root_node_name);
    // xml_read_libxml2.set_root_node_prefix(root_prefix);

    int result = xml_read_libxml2.read(file);
    BOOST_REQUIRE_EQUAL(result, 0);

    esysfile::xml::poco::File xml_read_poco;

    xml_read_poco.set_root_node_name(root_node_name);
    // xml_read_poco.set_root_node_namespace(root_prefix);
    result = xml_read_poco.read(file);
    BOOST_REQUIRE_EQUAL(result, 0);

    BOOST_REQUIRE_EQUAL(*xml_read_libxml2.get_data(), *xml_read_poco.get_data());
}

ESYSTEST_AUTO_TEST_CASE(Read01CompXML)
{
    boost::filesystem::path temp_path;
    boost::filesystem::path path;

    temp_path = TestCaseCtrl::get().GetTestFilesFolder();

    path = temp_path / "plant_catalog.xml";
    read_comp_file(path.string(), "CATALOG");

    path = temp_path / "sdk.esysconf";
    read_comp_file(path.string(), "esyssdk_conf");

    path = temp_path / "sap_test01.xml";
    read_comp_file(path.string(), "soap:Envelope");
}

} // namespace readcomp01

} // namespace xml

} // namespace esysfile_t
