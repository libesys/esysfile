/*!
 * \file esysfile_t/xml/filewrite02.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <esysfile/xml/data.h>
#include <esysfile/xml/poco/file.h>
#include <esysfile/xml/attr.h>

#include <iostream>

namespace esysfile_t
{

namespace xml
{

namespace xmlfilewrite02
{

void create_data(std::shared_ptr<esysfile::xml::Data> data)
{
    std::shared_ptr<esysfile::xml::Element> el;
    std::shared_ptr<esysfile::xml::Element> child;

    el = std::make_shared<esysfile::xml::Element>();
    el->set_name("directories");
    data->add_element(el);

    child = std::make_shared<esysfile::xml::Element>();
    child->set_name("directory");
    child->set_value("my directory");
    child->add_attr("BOOST", "c:\\prog\\boost");
    el->add_element(child);

    child = std::make_shared<esysfile::xml::Element>();
    child->set_name("directory");
    child->add_attr("WXWIN", "c:\\prog\\wx300");
    el->add_element(child);
}

void test_data(std::shared_ptr<esysfile::xml::Data> data)
{
    BOOST_REQUIRE_EQUAL(1, data->get_element_count());
    auto pel = data->get_element(0);
    BOOST_REQUIRE_NE(pel, nullptr);
    BOOST_REQUIRE_EQUAL("directories", pel->get_name());
    BOOST_REQUIRE_EQUAL(0, pel->get_attr_count());
    BOOST_REQUIRE_EQUAL(2, pel->get_element_count());

    auto pchild = pel->get_element(0);
    BOOST_REQUIRE_NE(pchild, nullptr);
    BOOST_REQUIRE_EQUAL(1, pchild->get_attr_count());
    BOOST_REQUIRE_EQUAL(true, pchild->has_value());

    auto attr = pchild->get_attr(0);
    BOOST_REQUIRE_NE(attr, nullptr);
    BOOST_REQUIRE_EQUAL("BOOST", attr->get_name());
    BOOST_REQUIRE_EQUAL("c:\\prog\\boost", attr->get_value());
    BOOST_REQUIRE_EQUAL(0, pchild->get_element_count());

    pchild = pel->get_element(1);
    BOOST_REQUIRE_NE(pchild, nullptr);
    BOOST_REQUIRE_EQUAL(1, pchild->get_attr_count());
    attr = pchild->get_attr(0);
    BOOST_REQUIRE_NE(attr, nullptr);
    BOOST_REQUIRE_EQUAL("WXWIN", attr->get_name());
    BOOST_REQUIRE_EQUAL("c:\\prog\\wx300", attr->get_value());
    BOOST_REQUIRE_EQUAL(0, pchild->get_element_count());
}

ESYSTEST_AUTO_TEST_CASE(XMLFileWrite02)
{
    esysfile::xml::poco::File xml_write;

    xml_write.set_root_node_name("xmlfilewrite02");
    create_data(xml_write.get_data());

    boost::filesystem::path path;

    path = TestCaseCtrl::get().GetTempFilesFolder();
    path /= "xmlfilewrite02.xml";

    int result = xml_write.write(path.string());
    BOOST_REQUIRE_EQUAL(result, 0);

    esysfile::xml::poco::File xml_read;

    xml_read.set_root_node_name("xmlfilewrite02");
    result = xml_read.read(path.string());
    BOOST_REQUIRE_EQUAL(result, 0);

    test_data(xml_read.get_data());
}

} // namespace xmlfilewrite02

} // namespace xml

} // namespace esysfile_t
