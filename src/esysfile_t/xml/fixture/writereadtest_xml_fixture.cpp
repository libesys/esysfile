/*!
 * \file esysfile_t/xml/fixture/writetest_xml_fixture.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"
#include "esysfile_t/xml/fixture/writereadtest.h"

#include <boost/filesystem.hpp>

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

WriteReadTest::WriteReadTest(const std::string &filename, esysfile::xml::FileBase *xml_write,
                             esysfile::xml::FileBase *xml_read, xml::fixture::DataBase *data)
    : m_filename(filename)
    , m_xml_write(xml_write)
    , m_xml_read(xml_read)
    , m_data(data)
{
}

WriteReadTest::~WriteReadTest()
{
}

void WriteReadTest::set_filename(const std::string &filename)
{
    m_filename = filename;
}

const std::string &WriteReadTest::get_filename() const
{
    return m_filename;
}

void WriteReadTest::set_xml_write(esysfile::xml::FileBase *xml_write)
{
    m_xml_write = xml_write;
}

esysfile::xml::FileBase *WriteReadTest::get_xml_write() const
{
    return m_xml_write;
}

void WriteReadTest::set_xml_read(esysfile::xml::FileBase *xml_read)
{
    m_xml_read = xml_read;
}

esysfile::xml::FileBase *WriteReadTest::get_xml_read() const
{
    return m_xml_read;
}

void WriteReadTest::set_data(esysfile_t::xml::fixture::DataBase *data)
{
    m_data = data;
}

esysfile_t::xml::fixture::DataBase *WriteReadTest::get_data() const
{
    return m_data;
}

void WriteReadTest::run()
{
    ESYSTEST_REQUIRE_NE(get_xml_write(), nullptr);
    ESYSTEST_REQUIRE_NE(m_data, nullptr);

    get_xml_write()->set_root_node_name(m_data->get_root_name());
    m_data->create(get_xml_write()->get_data());

    boost::filesystem::path path;

    path = TestCaseCtrl::get().GetTempFilesFolder();
    path /= get_filename();

    int result = get_xml_write()->write(path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    ESYSTEST_REQUIRE_NE(get_xml_read(), nullptr);

    get_xml_read()->set_root_node_name(m_data->get_root_name());
    result = get_xml_read()->read(path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    m_data->test(get_xml_read()->get_data());
}

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
