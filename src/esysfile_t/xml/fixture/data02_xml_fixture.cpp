/*!
 * \file esysfile_t/xml/fixture/data02_xml_fixture.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/xml/fixture/data02.h"

#include <esysfile/xml/attr.h>

#include <ctime>

using namespace std::literals;

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

Data02::Data02()
    : DataBase("data02")
{
}

Data02::~Data02()
{
}

void Data02::set_measurements_count(std::size_t measurements_count)
{
    m_measurements_count = measurements_count;
}

std::size_t Data02::get_measurements_count() const
{
    return m_measurements_count;
}

std::chrono::system_clock::time_point &Data02::get_meas_time()
{
    return m_meas_time;
}

const std::chrono::system_clock::time_point &Data02::get_meas_time() const
{
    return m_meas_time;
}

void Data02::add_device(std::shared_ptr<esysfile::xml::Element> devices_el)
{
    std::shared_ptr<esysfile::xml::Element> device_el;

    device_el = std::make_shared<esysfile::xml::Element>();
    device_el->set_name("device");
    device_el->add_attr("serial_number", "X12345ST");
    devices_el->add_element(device_el);

    add_settings(device_el);
}

void Data02::add_settings(std::shared_ptr<esysfile::xml::Element> device_el)
{
    std::shared_ptr<esysfile::xml::Element> settings_el;

    settings_el = std::make_shared<esysfile::xml::Element>();
    settings_el->set_name("settings");
    device_el->add_element(settings_el);

    std::shared_ptr<esysfile::xml::Element> cycle_el;

    cycle_el = std::make_shared<esysfile::xml::Element>();
    cycle_el->set_name("cycle_duration");
    cycle_el->set_value("60");
    settings_el->add_element(cycle_el);
}

void Data02::add_measurements(std::shared_ptr<esysfile::xml::Element> device_el)
{
    std::shared_ptr<esysfile::xml::Element> measurements_el;

    measurements_el = std::make_shared<esysfile::xml::Element>();
    measurements_el->set_name("measurements");
    device_el->add_element(measurements_el);

    for (std::size_t idx = 0; idx < get_measurements_count(); ++idx) add_measurement(measurements_el);
}

void Data02::add_measurement(std::shared_ptr<esysfile::xml::Element> measurements_el)
{
    std::shared_ptr<esysfile::xml::Element> meas_el;

    meas_el = std::make_shared<esysfile::xml::Element>();
    meas_el->set_name("meas");
    auto &meas_t = get_meas_time();
    auto t = std::chrono::system_clock::to_time_t(meas_t);
    char meas_t_str[100];

    std::strftime(meas_t_str, sizeof(meas_t_str) - 1, "%Y.%m.%d %H:%M:%S", std::localtime(&t));
    meas_el->add_attr("time", meas_t_str);

    meas_t += 60s;

    meas_el->add_attr("value", "25.1");
    meas_el->add_attr("unit", "C");
    measurements_el->add_element(meas_el);
}

void Data02::create(std::shared_ptr<esysfile::xml::Data> data)
{
    std::shared_ptr<esysfile::xml::Element> el;
    std::shared_ptr<esysfile::xml::Element> child;

    data->set_root_node_name(get_root_name());

    el = std::make_shared<esysfile::xml::Element>();
    el->set_name("devices");
    data->add_element(el);

    auto device = std::make_shared<esysfile::xml::Element>();
    device->set_name("device");
    device->add_attr("serial_number", "X12345ST");
    el->add_element(device);

    add_settings(device);
    add_measurements(device);
}

void Data02::test(std::shared_ptr<esysfile::xml::Data> data)
{
    ESYSTEST_REQUIRE_EQUAL(1, data->get_element_count());
    auto pel = data->get_element(0);
    ESYSTEST_REQUIRE_NE(pel, nullptr);
    ESYSTEST_REQUIRE_EQUAL("directories", pel->get_name());
    ESYSTEST_REQUIRE_EQUAL(0, pel->get_attr_count());
    ESYSTEST_REQUIRE_EQUAL(2, pel->get_element_count());

    auto pchild = pel->get_element(0);
    ESYSTEST_REQUIRE_NE(pchild, nullptr);
    ESYSTEST_REQUIRE_EQUAL(1, pchild->get_attr_count());
    auto attr = pchild->get_attr(0);
    ESYSTEST_REQUIRE_NE(attr, nullptr);
    ESYSTEST_REQUIRE_EQUAL("BOOST", attr->get_name());
    ESYSTEST_REQUIRE_EQUAL("c:\\prog\\boost", attr->get_value());
    ESYSTEST_REQUIRE_EQUAL(0, pchild->get_element_count());

    pchild = pel->get_element(1);
    ESYSTEST_REQUIRE_NE(pchild, nullptr);
    ESYSTEST_REQUIRE_EQUAL(1, pchild->get_attr_count());
    attr = pchild->get_attr(0);
    ESYSTEST_REQUIRE_NE(attr, nullptr);
    ESYSTEST_REQUIRE_EQUAL("WXWIN", attr->get_name());
    ESYSTEST_REQUIRE_EQUAL("c:\\prog\\wx300", attr->get_value());
    ESYSTEST_REQUIRE_EQUAL(0, pchild->get_element_count());
}

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
