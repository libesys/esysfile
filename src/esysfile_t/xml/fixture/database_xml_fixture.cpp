/*!
 * \file esysfile_t/xml/fixture/database_xml_fixture.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/xml/fixture/database.h"

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

DataBase::DataBase(const std::string &root_name)
    : m_root_name(root_name)
{
}

DataBase::~DataBase()
{
}

void DataBase::set_root_name(const std::string &root_name)
{
    m_root_name = root_name;
}

const std::string &DataBase::get_root_name() const
{
    return m_root_name;
}

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
