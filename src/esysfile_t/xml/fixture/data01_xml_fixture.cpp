/*!
 * \file esysfile_t/xml/fixture/data01_xml_fixture.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/xml/fixture/data01.h"

#include <esysfile/xml/attr.h>

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

Data01::Data01()
    : DataBase("data01")
{
}

Data01::~Data01()
{
}

void Data01::create(std::shared_ptr<esysfile::xml::Data> data)
{
    std::shared_ptr<esysfile::xml::Element> el;
    std::shared_ptr<esysfile::xml::Element> child;

    el = std::make_shared<esysfile::xml::Element>();
    el->set_name("directories");
    data->add_element(el);

    child = std::make_shared<esysfile::xml::Element>();
    child->set_name("directory");
    child->add_attr("BOOST", "c:\\prog\\boost");
    el->add_element(child);

    child = std::make_shared<esysfile::xml::Element>();
    child->set_name("directory");
    child->add_attr("WXWIN", "c:\\prog\\wx300");
    el->add_element(child);
}

void Data01::test(std::shared_ptr<esysfile::xml::Data> data)
{
    ESYSTEST_REQUIRE_EQUAL(1, data->get_element_count());
    auto pel = data->get_element(0);
    ESYSTEST_REQUIRE_NE(pel, nullptr);
    ESYSTEST_REQUIRE_EQUAL("directories", pel->get_name());
    ESYSTEST_REQUIRE_EQUAL(0, pel->get_attr_count());
    ESYSTEST_REQUIRE_EQUAL(2, pel->get_element_count());

    auto pchild = pel->get_element(0);
    ESYSTEST_REQUIRE_NE(pchild, nullptr);
    ESYSTEST_REQUIRE_EQUAL(1, pchild->get_attr_count());
    auto attr = pchild->get_attr(0);
    ESYSTEST_REQUIRE_NE(attr, nullptr);
    ESYSTEST_REQUIRE_EQUAL("BOOST", attr->get_name());
    ESYSTEST_REQUIRE_EQUAL("c:\\prog\\boost", attr->get_value());
    ESYSTEST_REQUIRE_EQUAL(0, pchild->get_element_count());

    pchild = pel->get_element(1);
    ESYSTEST_REQUIRE_NE(pchild, nullptr);
    ESYSTEST_REQUIRE_EQUAL(1, pchild->get_attr_count());
    attr = pchild->get_attr(0);
    ESYSTEST_REQUIRE_NE(attr, nullptr);
    ESYSTEST_REQUIRE_EQUAL("WXWIN", attr->get_name());
    ESYSTEST_REQUIRE_EQUAL("c:\\prog\\wx300", attr->get_value());
    ESYSTEST_REQUIRE_EQUAL(0, pchild->get_element_count());
}

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
