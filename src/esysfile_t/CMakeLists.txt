#
# cmake-format: off
# __legal_b__
#
# Copyright (c) 2021 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
#
# __legal_e__
# cmake-format: on
#

project(esysfile_t CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(GNUInstallDirs)

add_definitions(-DESYSTEST_MULTIOS -DESYSTEST_USE_BOOST)
set(ESYSTEST_USE_BOOST 1)

include_directories(../../include)

set(ESYSFILE_T_CXX_SOURCES esysfile_t_prec.cpp main.cpp testcasectrl.cpp)

set(ESYSFILE_T_XML_CXX_SOURCES
    xml/filewrite01_xml.cpp xml/filewrite02_xml.cpp xml/readcomp01_xml.cpp
    xml/writeread01_xml.cpp xml/writeread02_xml.cpp)

set(ESYSFILE_T_XML_FIXTURE_CXX_SOURCES
    xml/fixture/data01_xml_fixture.cpp xml/fixture/data02_xml_fixture.cpp
    xml/fixture/database_xml_fixture.cpp
    xml/fixture/writereadtest_xml_fixture.cpp)

set(ESYSFILE_T_XML_LIBXML2_CXX_SOURCES
    xml/libxml2/read01_xml_libxml2.cpp
    xml/libxml2/readsigned01_xml_libxml2.cpp
    xml/libxml2/readsignwrite01_xml_libxml2.cpp
    xml/libxml2/writeread01_xml_libxml2.cpp
    xml/libxml2/writesigned01_xml_libxml2.cpp)

add_executable(
  esysfile_t
  ${ESYSFILE_T_CXX_SOURCES} ${ESYSFILE_T_XML_CXX_SOURCES}
  ${ESYSFILE_T_XML_FIXTURE_CXX_SOURCES} ${ESYSFILE_T_XML_LIBXML2_CXX_SOURCES})

find_package(
  Boost 1.66 REQUIRED QUIET COMPONENTS date_time filesystem iostreams
                                       program_options unit_test_framework)

target_link_libraries(esysfile_t PUBLIC esysfile)

target_link_libraries(esysfile_t PUBLIC esystest)

if(ESYSFILE_USE_CONAN)
  target_link_libraries(esysfile_t PRIVATE ${CONAN_LIBS})
else()
  target_link_libraries(
    esysfile_t PUBLIC Boost::filesystem Boost::program_options
                      Boost::unit_test_framework esysfile)

  target_link_libraries(esysfile_t PUBLIC esystest)

endif()

install(TARGETS esysfile_t RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

add_test(
  NAME esysfile_t
  WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
  COMMAND esysfile_t -l test_suite)

if(DEFINED ESYSTEST_USE_BOOST)
  add_definitions(-DBOOST_ALL_DYN_LINK)
  find_package(Boost 1.66 REQUIRED QUIET COMPONENTS unit_test_framework)
  target_link_libraries(esysfile_t PUBLIC Boost::unit_test_framework)
endif()
