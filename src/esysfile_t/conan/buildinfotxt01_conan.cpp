/*!
 * \file esysfile_t/conan/buildinfotxt01_conan.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <esysfile/conan/buildinfotxt.h>

#include <iostream>

namespace esysfile::conan::test::buildinfotxt01
{

ESYSTEST_AUTO_TEST_CASE(ConanBuildInfoTxt01)
{
    boost::filesystem::path path;

    path = esysfile_t::TestCaseCtrl::get().GetTestFilesFolder();
    path /= "conan";
    path /= "conanbuildinfo.txt";

    BuildInfoTxt file;

    int result = file.read(path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);
    ESYSTEST_REQUIRE_EQUAL(file.get_lib_build_infos().size(), 5);

    auto lib_info = file.find("libxml2");
    ESYSTEST_REQUIRE_NE(lib_info, nullptr);
    ESYSTEST_REQUIRE_EQUAL(lib_info->get_include_dirs().size(), 2);
    ESYSTEST_REQUIRE_EQUAL(
        lib_info->get_include_dirs()[0],
        "C:/Users/miche/.conan/data/libxml2/2.9.12/_/_/package/196621320e89b8e9dec9694fe908631833950c9d/include");

    ESYSTEST_REQUIRE_EQUAL(lib_info->get_lib_dirs().size(), 1);
    ESYSTEST_REQUIRE_EQUAL(
        lib_info->get_lib_dirs()[0],
        "C:/Users/miche/.conan/data/libxml2/2.9.12/_/_/package/196621320e89b8e9dec9694fe908631833950c9d/lib");

    ESYSTEST_REQUIRE_EQUAL(lib_info->get_bin_dirs().size(), 1);
    ESYSTEST_REQUIRE_EQUAL(
        lib_info->get_bin_dirs()[0],
        "C:/Users/miche/.conan/data/libxml2/2.9.12/_/_/package/196621320e89b8e9dec9694fe908631833950c9d/bin");
}

} // namespace esysfile::conan::test::buildinfotxt01
