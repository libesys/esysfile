/*!
 * \file esysfile/esysfile_t_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *Copyright (c) 2014-2023 Michel Gillet
 *Distributed under the MIT License.
 *(See accompanying file LICENSE.txt or
 *copy at https://opensource.org/licenses/MIT)
 *
 *__legal_e__
 * \endcond
 *
 */

// esysfile_t.cpp : source file that includes just the standard includes
// esysfile_t.pch will be the pre-compiled header
// esysfile_t.obj will contain the pre-compiled type information

#include "esysfile_t/esysfile_t_prec.h"

// TODO: reference any additional headers you need in esysfile_t_prec.h
// and not in this file
