/*!
 * \file esysfile/unitestexe.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifdef ESYS_BOOST_TEST_USE

#define BOOST_TEST_MAIN

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#pragma warning(disable : 4985)
#endif

#include <boost/test/unit_test.hpp>

#ifdef _MSC_VER
#pragma warning(default : 4996)
#pragma warning(default : 4985)
#endif

#else

#define ESYSTEST_TEST_MAIN

#include <esystest/unit_test.h>

#endif

#ifdef WIN32

#if ESYS_USE_VLD
#include <vld.h>
#endif

#endif
