/*!
 * \file esysfile_t/testcasectrl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/testcasectrl.h"

#include <boost/filesystem.hpp>

#include <iostream>
#include <cassert>

namespace esysfile_t
{

TestCaseCtrl *TestCaseCtrl::g_test_case = nullptr;

TestCaseCtrl &TestCaseCtrl::get()
{
    assert(g_test_case != nullptr);

    return *g_test_case;
}

TestCaseCtrl::TestCaseCtrl()
    : esystest::TestCaseCtrl()
{
    g_test_case = this;

    AddSearchPathEnvVar("ESYSFILE");
    AddSearchPath("res/esysfile_t");                    // If cwd is root of the emdev git repo
    AddSearchPath("../../src/esysfile/res/esysfile_t"); // if cwd is the bin folder

    m_logger.Set(&std::cout);
    esystest::Logger::Set(&m_logger);
}

TestCaseCtrl::~TestCaseCtrl()
{
}

const std::string TestCaseCtrl::delete_create_temp_folder(const std::string &name)
{
    boost::filesystem::path file_path = GetTempFilesFolder();
    file_path /= name;

    bool remove_all = true;

    try
    {
        if (boost::filesystem::exists(file_path)) boost::filesystem::remove_all(file_path.string());
    }
    catch (const boost::filesystem::filesystem_error &e)
    {
        remove_all = false;
        std::cerr << e.what() << std::endl;
    }

    if (!remove_all) return "";

    bool created = boost::filesystem::create_directory(file_path);
    if (!created) return "";
    return file_path.string();
}

ESYSTEST_GLOBAL_FIXTURE(TestCaseCtrl);

} // namespace esysfile_t
