/*!
 * \file esysfile_t/argsfixture.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile_t/esysfile_t_prec.h"
#include "esysfile_t/argsfixture.h"

#include <iostream>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

namespace po = boost::program_options;

ArgsFixture::ArgsFixture()
    : m_argc(boost::unit_test::framework::master_test_suite().argc)
    , m_argv(boost::unit_test::framework::master_test_suite().argv)
{
    po::options_description desc("Allowed options");
    std::string dft_test_file_path;
    boost::filesystem::path cur_path = boost::filesystem::absolute("");
    boost::filesystem::path test_path = cur_path.branch_path().branch_path();

    m_abs_temp_path = cur_path;
    m_abs_temp_path /= "temp";

    if (!boost::filesystem::exists(m_abs_temp_path))
    {
        boost::filesystem::create_directory(m_abs_temp_path);
    }

    test_path /= "res";
    test_path /= "esyscore_t";
    test_path /= "test_files";

    if (!boost::filesystem::exists(test_path))
        dft_test_file_path = test_path.string();
    else
    {
        test_path = cur_path.branch_path().branch_path().branch_path().branch_path();
        test_path /= "res";
        test_path /= "esyscore_t";
        test_path /= "test_files";

        // std::cout << test_path.string() << std::endl;

        if (boost::filesystem::exists(test_path))
            dft_test_file_path = test_path.string();
        else
            dft_test_file_path = "test_files";
    }
    desc.add_options()("help", "produce help message")(
        "test_file_path", po::value<std::string>(&m_test_file_path)->default_value(dft_test_file_path),
        "set the path for test files");

    po::variables_map vm;
    po::store(po::parse_command_line(m_argc, m_argv, desc), vm);
    po::notify(vm);

    if (vm.count("help"))
    {
        std::cout << desc << "\n";
        // return 1;
    }

    /*if (vm.count("test_file_path"))
    {
        std::cout << "test_file_path was set to " << vm["test_file_path"].as<std::string>() << ".\n";
    }
    else
    {
        std::cout << "test_file_path was not set.\n";
    } */

    m_abs_test_file_path = boost::filesystem::absolute(m_test_file_path);
    // std::cout << "abs_test_file_path = " << abs_test_file_path.string() << std::endl;
}
