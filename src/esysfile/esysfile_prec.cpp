/*!
 * \file esysfile/esysfile_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *Copyright (c) 2014-2023 Michel Gillet
 *Distributed under the MIT License.
 *(See accompanying file LICENSE.txt or
 *copy at https://opensource.org/licenses/MIT)
 *
 *__legal_e__
 * \endcond
 *
 */

// esysfile.cpp : source file that includes just the standard includes
// esysfile.pch will be the pre-compiled header
// esysfile.obj will contain the pre-compiled type information

#include "esysfile/esysfile_prec.h"

// TODO: reference any additional headers you need in esysfile_prec.h
// and not in this file
