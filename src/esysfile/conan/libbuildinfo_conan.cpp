/*!
 * \file esysfile/conan/libbuildinfo_conan.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/conan/libbuildinfo.h"

namespace esysfile::conan
{

LibBuildInfo::LibBuildInfo() = default;

LibBuildInfo::~LibBuildInfo() = default;

void LibBuildInfo::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &LibBuildInfo::get_name() const
{
    return m_name;
}

std::vector<std::string> &LibBuildInfo::get_include_dirs()
{
    return m_include_dirs;
}

const std::vector<std::string> &LibBuildInfo::get_include_dirs() const
{
    return m_include_dirs;
}

std::vector<std::string> &LibBuildInfo::get_lib_dirs()
{
    return m_lib_dirs;
}

const std::vector<std::string> &LibBuildInfo::get_lib_dirs() const
{
    return m_lib_dirs;
}

std::vector<std::string> &LibBuildInfo::get_bin_dirs()
{
    return m_bin_dirs;
}

const std::vector<std::string> &LibBuildInfo::get_bin_dirs() const
{
    return m_bin_dirs;
}

std::vector<std::string> &LibBuildInfo::get_res_dirs()
{
    return m_res_dirs;
}

const std::vector<std::string> &LibBuildInfo::get_build_dirs() const
{
    return m_build_dirs;
}

std::vector<std::string> &LibBuildInfo::get_build_dirs()
{
    return m_build_dirs;
}

const std::vector<std::string> &LibBuildInfo::get_libs() const
{
    return m_libs;
}

std::vector<std::string> &LibBuildInfo::get_libs()
{
    return m_libs;
}

const std::vector<std::string> &LibBuildInfo::get_system_libs() const
{
    return m_system_libs;
}

std::vector<std::string> &LibBuildInfo::get_system_libs()
{
    return m_system_libs;
}

const std::vector<std::string> &LibBuildInfo::get_res_dirs() const
{
    return m_res_dirs;
}

bool LibBuildInfo::operator==(const LibBuildInfo &other) const
{
    return equal(other);
}

bool LibBuildInfo::operator!=(const LibBuildInfo &other) const
{
    return !equal(other);
}

bool LibBuildInfo::equal(const LibBuildInfo &other) const
{
    if (get_name() != other.get_name()) return false;
    if (m_include_dirs.size() != other.m_include_dirs.size()) return false;
    for (auto idx = 0; idx < m_include_dirs.size(); ++idx)
    {
        if (m_include_dirs[idx] != other.m_include_dirs[idx]) return false;
    }
    return true;
}

} // namespace esysfile::conan
