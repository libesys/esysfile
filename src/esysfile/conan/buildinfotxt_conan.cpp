/*!
 * \file esysfile/conan/buildinfotxt_conan.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/conan/buildinfotxt.h"

namespace esysfile::conan
{

BuildInfoTxt::BuildInfoTxt() = default;

BuildInfoTxt::BuildInfoTxt(const std::string &file_path)
    : m_file_path(file_path)
{
}

BuildInfoTxt::~BuildInfoTxt() = default;

void BuildInfoTxt::set_file_path(const std::string &file_path)
{
    m_file_path = file_path;
}

const std::string &BuildInfoTxt::get_file_path() const
{
    return m_file_path;
}

std::shared_ptr<LibBuildInfo> BuildInfoTxt::get_lib_build_info()
{
    if (m_lib_build_info == nullptr) m_lib_build_info = std::make_shared<LibBuildInfo>();
    return m_lib_build_info;
}

std::vector<std::shared_ptr<LibBuildInfo>> &BuildInfoTxt::get_lib_build_infos()
{
    return m_lib_build_infos;
}

const std::vector<std::shared_ptr<LibBuildInfo>> &BuildInfoTxt::get_lib_build_infos() const
{
    return m_lib_build_infos;
}

std::map<std::string, std::shared_ptr<LibBuildInfo>> &BuildInfoTxt::get_lib_build_info_map()
{
    return m_lib_build_info_map;
}

const std::map<std::string, std::shared_ptr<LibBuildInfo>> &BuildInfoTxt::get_lib_build_info_map() const
{
    return m_lib_build_info_map;
}

std::shared_ptr<LibBuildInfo> BuildInfoTxt::find(const std::string &name)
{
    auto it = m_lib_build_info_map.find(name);
    if (it == m_lib_build_info_map.end()) return nullptr;
    return it->second;
}

int BuildInfoTxt::read(const std::string &file_path)
{
    if (!file_path.empty()) set_file_path(file_path);

    m_ini_file = std::make_unique<ini::File>();
    auto result = m_ini_file->read(get_file_path());
    if (result < 0) return result;

    for (auto section : m_ini_file->get_sections())
    {
        result = process_section(section);
        if (result < 0) return result;
    }

    return 0;
}

int BuildInfoTxt::process_section(std::shared_ptr<ini::Section> section)
{
    std::string lib_name;
    std::string section_kind;
    std::string name = section->get_name();
    auto idx = name.rfind("_");

    std::shared_ptr<LibBuildInfo> lib_info;

    if ((idx != std::string::npos) && (name != "system_libs"))
    {
        // This is the info of a given lib
        lib_name = name.substr(idx + 1);
        section_kind = name.substr(0, idx);
        lib_info = find_or_new(lib_name);
    }
    else
    {
        section_kind = name;
        lib_info = get_lib_build_info();
    }

    return process_section(section_kind, section, lib_info);
}

int BuildInfoTxt::process_section(const std::string &section_kind, std::shared_ptr<ini::Section> section,
                                  std::shared_ptr<LibBuildInfo> lib_info)
{
    if (section_kind == "includedirs")
    {
        for (auto key_value : section->get_key_values())
        {
            lib_info->get_include_dirs().push_back(key_value->get_name());
        }
    }
    else if (section_kind == "libdirs")
    {
        for (auto key_value : section->get_key_values())
        {
            lib_info->get_lib_dirs().push_back(key_value->get_name());
        }
    }
    else if (section_kind == "bindirs")
    {
        for (auto key_value : section->get_key_values())
        {
            lib_info->get_bin_dirs().push_back(key_value->get_name());
        }
    }
    else if (section_kind == "resdirs")
    {
        for (auto key_value : section->get_key_values())
        {
            lib_info->get_res_dirs().push_back(key_value->get_name());
        }
    }
    else if (section_kind == "builddirs")
    {
        for (auto key_value : section->get_key_values())
        {
            lib_info->get_build_dirs().push_back(key_value->get_name());
        }
    }
    else if (section_kind == "libs")
    {
        for (auto key_value : section->get_key_values())
        {
            lib_info->get_libs().push_back(key_value->get_name());
        }
    }
    else if (section_kind == "system_libs")
    {
        for (auto key_value : section->get_key_values())
        {
            lib_info->get_system_libs().push_back(key_value->get_name());
        }
    }
    else
    {
        // TBD
    }

    return 0;
}

std::shared_ptr<LibBuildInfo> BuildInfoTxt::find_or_new(const std::string &name)
{
    std::shared_ptr<LibBuildInfo> lib_info;

    auto it = m_lib_build_info_map.find(name);
    if (it == m_lib_build_info_map.end())
    {
        lib_info = std::make_shared<LibBuildInfo>();
        lib_info->set_name(name);
        m_lib_build_infos.push_back(lib_info);
        m_lib_build_info_map[name] = lib_info;
    }
    else
        lib_info = it->second;
    return lib_info;
}

bool BuildInfoTxt::operator==(const BuildInfoTxt &other) const
{
    return equal(other);
}

bool BuildInfoTxt::operator!=(const BuildInfoTxt &other) const
{
    return !equal(other);
}

bool BuildInfoTxt::equal(const BuildInfoTxt &other) const
{
    return false;
}

} // namespace esysfile::conan
