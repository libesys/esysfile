/*!
 * \file esysfile/ini/comment_ini.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/ini/comment.h"

#include <typeinfo>

namespace esysfile::ini
{

Comment::Comment()
    : Element(Element::Type::COMMENT)
{
}

Comment::~Comment() = default;

void Comment::set_text(const std::string &text)
{
    m_text = text;
}

const std::string &Comment::get_text() const
{
    return m_text;
}

void Comment::add_line(const std::string &line)
{
    m_text += line;
}

bool Comment::equal(const Element &other) const
{
    if (other.get_type() != Element::Type::COMMENT) return false;

    try
    {
        const Comment &other_comment = dynamic_cast<const Comment &>(other);
        return get_text() == other_comment.get_text();
    }
    catch (std::bad_cast)
    {
        return false;
    }
}

} // namespace esysfile::ini
