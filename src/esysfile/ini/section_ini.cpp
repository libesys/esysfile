/*!
 * \file esysfile/ini/section_ini.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/ini/section.h"

namespace esysfile::ini
{

Section::Section()
    : Element(Type::SECTION)
{
}

Section::~Section() = default;

void Section::add(std::shared_ptr<Comment> comment)
{
    m_elements.push_back(comment);
}

void Section::add(std::shared_ptr<KeyValue> key_value)
{
    m_elements.push_back(key_value);
    m_key_values.push_back(key_value);
    if (!key_value->get_key_only()) m_key_values_map[key_value->get_name()] = key_value;
}

void Section::add(std::shared_ptr<Section> section)
{
    m_sections.push_back(section);
    m_sections_map[section->get_name()] = section;
}

std::vector<std::shared_ptr<Element>> &Section::get_elements()
{
    return m_elements;
}

const std::vector<std::shared_ptr<Element>> &Section::get_elements() const
{
    return m_elements;
}

std::vector<std::shared_ptr<KeyValue>> &Section::get_key_values()
{
    return m_key_values;
}

const std::vector<std::shared_ptr<KeyValue>> &Section::get_key_values() const
{
    return m_key_values;
}

std::map<std::string, std::shared_ptr<KeyValue>> &Section::get_key_values_map()
{
    return m_key_values_map;
}

const std::map<std::string, std::shared_ptr<KeyValue>> &Section::get_key_values_map() const
{
    return m_key_values_map;
}

std::vector<std::shared_ptr<Section>> &Section::get_sections()
{
    return m_sections;
}

const std::vector<std::shared_ptr<Section>> &Section::get_sections() const
{
    return m_sections;
}

std::map<std::string, std::shared_ptr<Section>> &Section::get_sections_map()
{
    return m_sections_map;
}

const std::map<std::string, std::shared_ptr<Section>> &Section::get_sections_map() const
{
    return m_sections_map;
}

bool Section::equal(const Element &other) const
{
    if (get_name() != other.get_name()) return false;
    if (get_type() != other.get_type()) return false;

    try
    {
        auto other_typed = dynamic_cast<const Section &>(other);
        for (auto idx = 0; idx < get_elements().size(); ++idx)
        {
            if (*get_elements()[idx] != *other_typed.get_elements()[idx]) return false;
        }
    }
    catch (std::bad_cast)
    {
        return false;
    }

    return true;
}

} // namespace esysfile::ini
