/*!
 * \file esysfile/ini/element_ini.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/ini/element.h"

namespace esysfile::ini
{

Element::Element() = default;

Element::Element(Type type)
    : m_type(type)
{
}

Element::~Element() = default;

void Element::set_type(Type type)
{
    m_type = type;
}

Element::Type Element::get_type() const
{
    return m_type;
}

Element::Type &Element::get_type()
{
    return m_type;
}

void Element::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &Element::get_name() const
{
    return m_name;
}

std::string &Element::get_name()
{
    return m_name;
}

bool Element::operator==(const Element &other) const
{
    return equal(other);
}

//! Not equal to comparison operator
bool Element::operator!=(const Element &other) const
{
    return !equal(other);
}

} // namespace esysfile::ini
