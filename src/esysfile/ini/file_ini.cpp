/*!
 * \file esysfile/ini/file_ini.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/ini/file.h"

#include <boost/algorithm/string.hpp>

#include <cassert>
#include <string>

namespace esysfile::ini
{

File::File() = default;

File::File(const std::string &file_path)
    : m_file_path(file_path)
{
}

File::~File() = default;

void File::set_file_path(const std::string &file_path)
{
    m_file_path = file_path;
}

int File::read(const std::string &file_path)
{
    if (!file_path.empty()) set_file_path(file_path);

    m_line_number = 0;

    m_ifs.open(file_path);
    if (!m_ifs) return -1;

    while (fetch_line())
    {
        int result = process_line();
        if (result < 0) return result;
    }

    m_ifs.close();
    return 0;
}

int File::process_line()
{
    switch (get_cur_el_type())
    {
        case Element::Type::NOT_SET: return process_line_el_type_not_set();
        case Element::Type::SECTION: return process_line_el_type_section();
        case Element::Type::COMMENT: return process_line_el_type_comment();
        default: return -1;
    }
}

int File::process_line_el_type_not_set()
{
    std::shared_ptr<Comment> comment;
    std::shared_ptr<Section> section;
    bool is_sub = false;
    int result = 0;

    switch (get_line_el_type())
    {
        case Element::Type::COMMENT:
            set_cur_el_type(Element::Type::COMMENT);
            comment = std::make_shared<Comment>();
            comment->add_line(get_line());
            set_cur_comment(comment);
            m_elements.push_back(comment);
            break;
        case Element::Type::SECTION:
            set_cur_el_type(Element::Type::SECTION);
            section = std::make_shared<Section>();
            result = get_section_name_from_line(section->get_name(), is_sub);
            if (result < 0) return result;
            if (!is_sub)
            {
                push_back_cur_section(section);
                m_sections.push_back(section);
                m_sections_map[section->get_name()] = section;
            }
            else
            {
                return -1;
            }
            break;
        case Element::Type::NOT_SET: set_cur_comment(nullptr); break;
        default: return 0;
    }
    return 0;
}

int File::process_line_el_type_section()
{
    std::shared_ptr<Comment> comment;
    std::shared_ptr<Section> section;
    std::shared_ptr<KeyValue> key_value;
    std::string str;
    bool is_sub = false;
    int result = 0;

    assert(get_cur_section() != nullptr);

    switch (get_line_el_type())
    {
        case Element::Type::COMMENT:

            if (get_cur_comment() != nullptr)
                get_cur_comment()->add_line(get_line());
            else
            {
                comment = std::make_shared<Comment>();
                comment->set_text(get_line());
                get_cur_section()->add(comment);
            }
            break;
        case Element::Type::NOT_SET:
            str = get_line();
            boost::algorithm::trim(str);
            set_cur_comment(nullptr);
            if (!str.empty())
            {
                key_value = std::make_shared<KeyValue>();
                result = process_line_key_value(get_line(), key_value);
                if (result < 0) return result;
                get_cur_section()->add(key_value);
            }
            break;
        case Element::Type::SECTION:
            section = std::make_shared<Section>();
            result = get_section_name_from_line(section->get_name(), is_sub);
            if (result < 0) return result;
            if (!is_sub)
            {
                pop_cur_section();
                push_back_cur_section(section);
                m_sections.push_back(section);
                m_sections_map[section->get_name()] = section;
            }
            else
            {
                get_cur_section()->add(section);
                push_back_cur_section(section);
            }
            break;
    }
    return 0;
}

int File::process_line_key_value(const std::string &line, std::shared_ptr<KeyValue> key_value)
{
    std::string input = line;
    std::string key;
    std::string value;
    std::string comment;
    auto idx = line.find("=");
    if (idx == std::string::npos)
    {
        // It's only a key, no value
        key = input;
        key_value->set_key_only(true);
    }
    else
    {
        key = input.substr(0, idx);
        value = input.substr(idx + 1);
        boost::algorithm::trim(key);
        boost::algorithm::trim(value);

        idx = value.find(";");
        if (idx == std::string::npos)
        {
            idx = value.find("#");
        }

        if (idx != std::string::npos)
        {
            value = value.substr(0, idx);
            comment = value.substr(idx);
        }
        key_value->set_value(value);
        key_value->set_comment(comment);
    }
    key_value->set_name(key);
    return 0;
}

int File::process_line_el_type_comment()
{
    return 0;
}

Element::Type File::get_line_el_type() const
{
    if (m_line[0] == ';') return Element::Type::COMMENT;
    if (m_line[0] == '#') return Element::Type::COMMENT;
    if (m_line[0] == '[') return Element::Type::SECTION;
    return Element::Type::NOT_SET;
}

void File::set_cur_comment(std::shared_ptr<Comment> cur_comment)
{
    m_cur_comment = cur_comment;
}

std::shared_ptr<Comment> File::get_cur_comment() const
{
    return m_cur_comment;
}

void File::push_back_cur_section(std::shared_ptr<Section> section)
{
    m_cur_sections.push_back(section);
}

std::shared_ptr<Section> File::get_cur_section() const
{
    return m_cur_sections.back();
}
void File::pop_cur_section()
{
    m_cur_sections.pop_back();
}

int File::get_section_name_from_line(std::string &section_name, bool &is_sub) const
{
    return get_section_name_from_line(m_line, section_name, is_sub);
}

int File::get_section_name_from_line(const std::string &line, std::string &section_name, bool &is_sub) const
{
    if (line[0] != '[') return -1;
    std::string sec_name = line.substr(1);
    auto idx = sec_name.find("]");
    if (idx == std::string::npos) return -1;

    section_name = sec_name.substr(0, idx);
    idx = section_name.find(".");
    if (idx != std::string::npos)
        is_sub = true;
    else
        is_sub = false;
    return 0;
}

void File::set_cur_el_type(Element::Type cur_el_type)
{
    m_cur_el_type = cur_el_type;
}

Element::Type File::get_cur_el_type() const
{
    return m_cur_el_type;
}

const std::string &File::get_file_path() const
{
    return m_file_path;
}

std::vector<std::shared_ptr<Element>> &File::get_elements()
{
    return m_elements;
}

const std::vector<std::shared_ptr<Element>> &File::get_elements() const
{
    return m_elements;
}

std::vector<std::shared_ptr<Section>> &File::get_sections()
{
    return m_sections;
}
const std::vector<std::shared_ptr<Section>> &File::get_sections() const
{
    return m_sections;
}

std::map<std::string, std::shared_ptr<Section>> &File::get_sections_map()
{
    return m_sections_map;
}
const std::map<std::string, std::shared_ptr<Section>> &File::get_sections_map() const
{
    return m_sections_map;
}

std::shared_ptr<Section> File::find_section(const std::string &name) const
{
    auto it = m_sections_map.find(name);
    if (it == m_sections_map.end()) return nullptr;
    return it->second;
}

bool File::operator==(const File &other) const
{
    return equal(other);
}

//! Not equal to comparison operator
bool File::operator!=(const File &other) const
{
    return !equal(other);
}

bool File::fetch_line()
{
    ++m_line_number;
    if (std::getline(m_ifs, m_line)) return true;
    return false;
}

const std::string &File::get_line() const
{
    return m_line;
}

bool File::equal(const File &other) const
{
    if (get_elements().size() != other.get_elements().size()) return false;

    for (auto idx = 0; idx < get_elements().size(); ++idx)
    {
        if ((get_elements()[idx] == nullptr) || (other.get_elements()[idx] == nullptr)) return false;
        if (*get_elements()[idx] != *other.get_elements()[idx]) return false;
    }

    return true;
}

} // namespace esysfile::ini
