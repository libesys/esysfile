/*!
 * \file esysfile/ini/keyvalue_ini.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/ini/keyvalue.h"

namespace esysfile::ini
{

KeyValue::KeyValue()
    : Element(Type::KEYVALUE)
{
}

KeyValue::~KeyValue() = default;

void KeyValue::set_key_only(bool key_only)
{
    m_key_only = key_only;
}

bool KeyValue::get_key_only() const
{
    return m_key_only;
}

void KeyValue::set_value(const std::string &value)
{
    m_value = value;
}

const std::string &KeyValue::get_value() const
{
    return m_value;
}

void KeyValue::set_comment(const std::string &comment)
{
    m_comment = comment;
}

const std::string &KeyValue::get_comment() const
{
    return m_comment;
}

bool KeyValue::equal(const Element &other) const
{
    if (get_name() != other.get_name()) return false;

    return true;
}

} // namespace esysfile::ini
