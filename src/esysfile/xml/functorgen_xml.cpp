/*!
 * \file esysfile/xml/functorgen.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/functorgen.h"

namespace esysfile
{

namespace xml
{

FunctorGen::FunctorGen()
{
}

FunctorGen::~FunctorGen()
{
}

} // namespace xml

} // namespace esysfile
