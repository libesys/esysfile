/*!
 * \file esysfile/xml/element.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/attr.h"

#include <Poco/DOM/NamedNodeMap.h>
#include <Poco/DOM/Text.h>
#include <Poco/DOM/Attr.h>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <cassert>

namespace esysfile
{

namespace xml
{

int Element::s_count = 0;
int Element::s_max_count = 0;

int Element::get_count()
{
    return s_count;
}

int Element::get_max_count()
{
    return s_max_count;
}

Element::Element(Data *data)
    : m_data(data)
{
    m_id = s_max_count;
    ++s_max_count;
    ++s_count;
}

Element::~Element()
{
    --s_count;
    clear();
}

void Element::clear()
{
    get_elements().clear();
    get_attrs().clear();
}

void Element::set_name(const std::string &name)
{
    std::string el_name = name;
    std::string el_prefix;

    auto i = name.find(":");

    if (i != std::string::npos)
    {
        set_prefix(name.substr(0, i));
        m_name = name.substr(i + 1);
    }
    else
        m_name = name;
}

std::string &Element::get_name()
{
    return m_name;
}

const std::string &Element::get_name() const
{
    return m_name;
}

void Element::set_prefix(const std::string &prefix)
{
    m_prefix = prefix;
}

std::string &Element::get_prefix()
{
    return m_prefix;
}

const std::string &Element::get_prefix() const
{
    return m_prefix;
}

std::string Element::get_fullname() const
{
    if (get_prefix().empty()) return get_name();
    return get_prefix() + ":" + get_name();
}

std::shared_ptr<Element> Element::add_element(const std::string &name)
{
    std::shared_ptr<Element> el = std::make_shared<Element>();

    el->set_name(name);

    add_element(el);

    return el;
}

void Element::add_element(std::shared_ptr<Element> el)
{
    std::map<std::string, std::shared_ptr<Element>>::iterator map_it;
    std::map<std::string, std::vector<std::shared_ptr<Element>>>::iterator list_map_it;

    int cur_depth = get_depth();

    m_elements.push_back(el);
    el->set_depth(cur_depth + 1);
    el->set_parent(shared_from_this());

    map_it = m_elements_map.find(el->get_name());
    if (map_it == m_elements_map.end())
        m_elements_map[el->get_name()] = el;
    else
    {
        list_map_it = m_list_elements_map.find(el->get_name());
        if (list_map_it == m_list_elements_map.end())
        {
            std::vector<std::shared_ptr<Element>> vec;

            vec.push_back(el);
            m_list_elements_map[el->get_name()] = vec;
        }
        else
        {
            list_map_it->second.push_back(el);
        }
    }
}

void Element::add_element(std::shared_ptr<Element> parent, const std::string &xml_name, const std::string &value)
{
    std::shared_ptr<Element> el;

    el = std::make_shared<Element>();
    el->set_name(xml_name);
    el->set_value(value);

    parent->add_element(el);
}

void Element::add_element(std::shared_ptr<Element> parent, const std::string &xml_name,
                          const std::vector<std::string> &values, bool comma_separated)
{
    std::string values_str;

    std::size_t idx = 0;

    for (auto &value : values)
    {
        if (idx != 0)
        {
            if (comma_separated)
                values_str += ", ";
            else
                values_str += " ";
        }
        values_str += value;
        ++idx;
    }

    add_element(parent, xml_name, values_str);
}

std::size_t Element::get_element_count()
{
    return m_elements.size();
}

std::size_t Element::get_element_count(const std::string &name)
{
    std::map<std::string, std::shared_ptr<Element>>::iterator map_it;
    std::map<std::string, std::vector<std::shared_ptr<Element>>>::iterator list_map_it;
    std::size_t result = 0;

    map_it = m_elements_map.find(name);
    if (map_it != m_elements_map.end()) result++;
    list_map_it = m_list_elements_map.find(name);
    if (list_map_it != m_list_elements_map.end()) result += list_map_it->second.size();
    return result;
}

std::shared_ptr<Element> Element::get_element(std::size_t idx)
{
    if (idx >= m_elements.size()) return nullptr;
    return m_elements[idx];
}

std::shared_ptr<Element> Element::get_element(const std::string &name, std::size_t idx)
{
    std::map<std::string, std::shared_ptr<Element>>::iterator map_it;
    std::map<std::string, std::vector<std::shared_ptr<Element>>>::iterator list_map_it;
    int result = 0;

    if (idx == 0)
    {
        map_it = m_elements_map.find(name);
        if (map_it == m_elements_map.end()) return nullptr;
        return map_it->second;
    }

    list_map_it = m_list_elements_map.find(name);
    if (list_map_it == m_list_elements_map.end()) return nullptr;
    if ((idx - 1) >= list_map_it->second.size()) return nullptr;
    return list_map_it->second[idx - 1];
}

std::vector<std::shared_ptr<Element>> &Element::get_elements()
{
    return m_elements;
}

const std::vector<std::shared_ptr<Element>> &Element::get_elements() const
{
    return m_elements;
}

std::shared_ptr<Element> Element::get_element_by_path(const std::string &path)
{
    std::vector<std::string> results;
    std::shared_ptr<Element> el = shared_from_this();
    std::shared_ptr<Element> child_el;

    // *INDENT-OFF*
    boost::split(results, path, [](char c) { return c == '.'; });
    // *INDENT-ON*

    for (std::size_t idx = 0; idx < results.size(); ++idx)
    {
        child_el = el->get_element(results[idx]);
        if (child_el != nullptr)
            el = child_el;
        else
            return nullptr;
    }

    return el;
}

void Element::set_data(Data *data)
{
    m_data = data;
}

Data *Element::get_data()
{
    return m_data;
}

void Element::add_attr(std::shared_ptr<Attr> attr)
{
    m_attrs.push_back(attr);
}

void Element::add_attr(const std::string &name, const char *value)
{
    add_attr(name, std::string(value));
}

void Element::add_attr(const std::string &name, const std::string &value)
{
    std::shared_ptr<Attr> attr;

    attr = std::make_shared<Attr>();
    attr->set_name(name);
    attr->set_value(value);
    add_attr(attr);
}

void Element::add_attr(const std::string &name, int value)
{
    std::shared_ptr<Attr> attr;

    attr = std::make_shared<Attr>();
    attr->set_name(name);
    attr->set_value(value);
    add_attr(attr);
}

void Element::add_attr(const std::string &name, double value)
{
    auto attr = std::make_shared<Attr>();

    attr->set_name(name);
    attr->set_value(value);
    add_attr(attr);
}

void Element::add_attr(const std::string &name, bool value)
{
    auto attr = std::make_shared<Attr>();

    attr->set_name(name);
    attr->set_value(value);
    add_attr(attr);
}

std::size_t Element::get_attr_count()
{
    return m_attrs.size();
}

std::shared_ptr<Attr> Element::get_attr(std::size_t idx)
{
    if (idx >= m_attrs.size()) return nullptr;
    return m_attrs[idx];
}

std::shared_ptr<Attr> Element::get_attr(const std::string &name) const
{
    std::vector<std::shared_ptr<Attr>>::const_iterator it;

    for (it = m_attrs.begin(); it != m_attrs.end(); ++it)
        if ((*it)->get_name() == name) return *it;
    return nullptr;
}

std::vector<std::shared_ptr<Attr>> &Element::get_attrs()
{
    return m_attrs;
}

const std::vector<std::shared_ptr<Attr>> &Element::get_attrs() const
{
    return m_attrs;
}

int Element::get_path(Path &path)
{
    path.get_items().clear();

    std::shared_ptr<Element> cur_el = shared_from_this();

    while (cur_el != nullptr)
    {
        path.push_front(cur_el);
        if (!cur_el->get_parent().expired())
            cur_el = cur_el->get_parent().lock();
        else
            break;
    }
    if (cur_el->get_depth() == 0) return 0;
    return -1;
}

int Element::get_path(std::shared_ptr<Path> path)
{
    if (path == nullptr) return -1;

    return get_path(*path.get());
}

void Element::set_parent(std::weak_ptr<Element> parent)
{
    m_parent = parent;
}

std::weak_ptr<Element> Element::get_parent() const
{
    return m_parent;
}

std::weak_ptr<Element> Element::get_parent()
{
    return m_parent;
}

bool Element::has_value()
{
    return !m_value.empty();
}

std::string &Element::get_value()
{
    return m_value;
}

const std::string &Element::get_value() const
{
    return m_value;
}

void Element::set_value(const std::string &value)
{
    m_value = value;
}

int Element::get_value(unsigned int &value)
{
    try
    {
        value = boost::lexical_cast<unsigned int>(get_value());
    }
    catch (const boost::bad_lexical_cast &)
    {
        return -1;
    }
    return 0;
}

int Element::get_value(int &value)
{
    try
    {
        value = boost::lexical_cast<int>(get_value());
    }
    catch (const boost::bad_lexical_cast &)
    {
        return -1;
    }
    return 0;
}

void Element::set_line_number(int line_number)
{
    m_line_number = line_number;
}

int Element::get_line_number() const
{
    return m_line_number;
}

bool Element::operator==(const Element &other) const
{
    if (get_name() != other.get_name()) return false;
    if (get_value() != other.get_value()) return false;
    if (get_attrs().size() != other.get_attrs().size()) return false;
    if (get_elements().size() != other.get_elements().size()) return false;

    for (std::size_t idx = 0; idx < get_attrs().size(); ++idx)
    {
        assert(get_attrs()[idx] != nullptr);
        assert(other.get_attrs()[idx] != nullptr);

        if (*get_attrs()[idx].get() != *other.get_attrs()[idx].get()) return false;
    }

    for (std::size_t idx = 0; idx < get_elements().size(); ++idx)
    {
        assert(get_elements()[idx] != nullptr);
        assert(other.get_elements()[idx] != nullptr);

        if (*get_elements()[idx].get() != *other.get_elements()[idx].get()) return false;
    }

    return true;
}

bool Element::operator!=(const Element &other) const
{
    return !operator==(other);
}

int Element::get_depth() const
{
    return m_depth;
}

void Element::set_depth(int depth)
{
    m_depth = depth;
}

void Element::inc_depth()
{
    ++m_depth;
}

} // namespace xml

} // namespace esysfile
