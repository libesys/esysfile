/*!
 * \file esysfile/xml/writer_xml.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/writer.h"
#include "esysfile/xml/attr.h"

#include <sstream>
#include <locale>

namespace esysfile
{

namespace xml
{

Writer::Writer(const std::string &filename)
    : m_filename(filename)
{
}

Writer::~Writer()
{
}

void Writer::set_filename(const std::string &filename)
{
    m_filename = filename;
}

const std::string &Writer::get_filename() const
{
    return m_filename;
}

void Writer::set_data(std::shared_ptr<Data> data)
{
    m_data = data;
}

std::shared_ptr<Data> Writer::get_data()
{
    return m_data;
}

const std::shared_ptr<Data> Writer::get_data() const
{
    return m_data;
}

int Writer::write(const std::string &filename, std::shared_ptr<Data> data)
{
    if (!filename.empty()) set_filename(filename);

    if (get_filename().empty()) return -1;

    m_ofs.open(get_filename().c_str());
    if (!m_ofs.is_open()) return -3;

    int result = write(m_ofs, data);
    m_ofs.close();
    return result;
}

int Writer::write(std::ostream &os, std::shared_ptr<Data> data)
{
    if (data != nullptr) set_data(data);

    if (get_data() == nullptr) return -1;

    write_xml_header(os);

    if (!get_data()->get_dtd().empty()) write_dtd(os);

    write_element(os, get_data());
    return 0;
}

void Writer::write_xml_header(std::ostream &os)
{
    os << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << std::endl;
}

void Writer::write_dtd(std::ostream &os)
{
    os << "<!DOCTYPE " << get_data()->get_root_node_name() << " SYSTEM \"" << get_data()->get_dtd() << "\">"
       << std::endl;
}

void Writer::write_idents(std::ostream &os)
{
    for (auto idx = 0; idx < m_cur_indent_idx; ++idx) os << m_indent_str;
}

void Writer::write_element(std::ostream &os, std::shared_ptr<Element> el)
{
    std::ostringstream oss;

    write_idents(os);

    oss << "<" << el->get_name();
    if (el->get_attr_count() != 0)
    {
        write_attributes(oss, el);
    }
    if (el->get_element_count() != 0)
    {
        oss << ">" << std::endl;
        os << oss.str();

        for (auto child_el : el->get_elements())
        {
            ++m_cur_indent_idx;
            write_element(os, child_el);
            --m_cur_indent_idx;
        }
        write_idents(os);
        os << "</" << el->get_name() << ">" << std::endl;
    }
    else if (!el->get_value().empty())
    {
        oss << ">";
        os << oss.str();

        os << encode(el->get_value());
        os << "</" << el->get_name() << ">" << std::endl;
    }
    else
    {
        if (is_full_element(el->get_name()))
        {
            oss << ">" << std::endl;
            oss << "</" << el->get_name();
            oss << ">" << std::endl;
            os << oss.str();
        }
        else
        {
            oss << "/>" << std::endl;
            os << oss.str();
        }
    }
}

void Writer::write_attributes(std::ostream &os, std::shared_ptr<Element> el)
{
    for (auto attr : el->get_attrs()) write_attribute(os, attr);
}

void Writer::write_attribute(std::ostream &os, std::shared_ptr<Attr> attr)
{
    os << " ";
    os << attr->get_name() << "=\"" << encode(attr->get_value()) << "\"";
}

void Writer::add_full_element(const std::string &full_element)
{
    m_map_full_elements[full_element] = true;
}

bool Writer::is_full_element(const std::string &full_element) const
{
    return (m_map_full_elements.find(full_element) != m_map_full_elements.end());
}

void Writer::set_escape_single_quote(bool escape_apos)
{
    m_escape_single_quote = escape_apos;
}

bool Writer::get_escape_single_quote() const
{
    return m_escape_single_quote;
}

void Writer::set_escape_double_quote(bool escape_double_quote)
{
    m_escape_double_quote = escape_double_quote;
}

bool Writer::get_escape_double_quote() const
{
    return m_escape_double_quote;
}

void Writer::set_indent(std::size_t indent)
{
    m_indent = indent;
    m_indent_str = "";
    for (auto idx = 0; idx < indent; ++idx) m_indent_str += " ";
}

std::size_t Writer::get_indent() const
{
    return m_indent;
}

std::string Writer::encode(const std::string &text)
{
    std::string buffer;
    buffer.reserve(text.size());
    for (size_t pos = 0; pos != text.size(); ++pos)
    {
        switch (text[pos])
        {
            case '&': buffer.append("&amp;"); break;
            case '\"':
                if (get_escape_double_quote())
                    buffer.append("&quot;");
                else
                    buffer.append("\"");
                break;
            case '\'':
                if (get_escape_single_quote())
                    buffer.append("&apos;");
                else
                    buffer.append("\'");
                break;
            case '<': buffer.append("&lt;"); break;
            case '>': buffer.append("&gt;"); break;
            default: buffer.append(&text[pos], 1); break;
        }
    }
    return buffer;
}

} // namespace xml

} // namespace esysfile
