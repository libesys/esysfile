/*!
 * \file esysfile/xml/filebase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/filebase.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/attr.h"

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>

#include <cassert>

namespace esysfile
{

namespace xml
{

FileBase::FileBase(const std::string &filename)
    : m_filename(filename)
{
}

FileBase::~FileBase()
{
}

void FileBase::set_file_name(const std::string &filename)
{
    m_filename = filename;
}

std::string &FileBase::get_file_name()
{
    return m_filename;
}

const std::string &FileBase::get_file_name() const
{
    return m_filename;
}

void FileBase::set_data(std::shared_ptr<Data> data)
{
    m_data = data;
}

const std::shared_ptr<Data> FileBase::get_data() const
{
    return m_data;
}

std::shared_ptr<Data> FileBase::get_data()
{
    return m_data;
}

void FileBase::set_root_node_name(const std::string &name)
{
    get_data_or_new()->set_root_node_name(name);
}

std::string FileBase::get_root_node_name() const
{
    if (get_data() == nullptr) return "";

    return get_data()->get_root_node_name();
}

void FileBase::set_root_node_prefix(const std::string &name_space)
{
    get_data_or_new()->set_root_node_prefix(name_space);
}

std::string &FileBase::get_root_node_prefix()
{
    return get_data_or_new()->get_root_node_prefix();
}

const std::string &FileBase::get_root_node_prefix() const
{
    if (get_data() == nullptr) return m_empty;

    return get_data()->get_root_node_prefix();
}

std::string FileBase::get_root_node_fullname() const
{
    if (get_data() == nullptr) return m_empty;

    return get_data()->get_root_node_fullname();
}

void FileBase::set_root_attr_version(const std::string &version)
{
    get_data_or_new()->set_root_attr_version(version);
}

std::string FileBase::get_root_attr_version() const
{
    if (get_data() == nullptr) return "";

    return get_data()->get_root_attr_version();
}

void FileBase::set_root_attr_name(const std::string &name)
{
    get_data_or_new()->set_root_attr_name(name);
}

std::string FileBase::get_root_attr_name() const
{
    if (get_data() == nullptr) return "";

    return get_data()->get_root_attr_name();
}

void FileBase::set_indent(const std::string &indent)
{
    m_indent = indent;
}

const std::string &FileBase::get_indent() const
{
    return m_indent;
}

std::string &FileBase::get_indent()
{
    return m_indent;
}

bool FileBase::get_signature_support() const
{
    return false;
}

void FileBase::set_key_file(const std::string &key_file)
{
    m_key_file = key_file;
}

const std::string &FileBase::get_key_file() const
{
    return m_key_file;
}

void FileBase::set_cert_file(const std::string &cert_file)
{
    m_cert_file = cert_file;
}

const std::string &FileBase::get_cert_file() const
{
    return m_cert_file;
}

void FileBase::set_digitally_signed(bool digitally_signed)
{
    m_digitally_signed = digitally_signed;
}

bool FileBase::get_digitally_signed() const
{
    return m_digitally_signed;
}

void FileBase::add_trusted_cert_file(const std::string &trusted_cert_file)
{
    m_trusted_cert_files.push_back(trusted_cert_file);
}

const std::vector<std::string> &FileBase::get_trusted_cert_files() const
{
    return m_trusted_cert_files;
}

void FileBase::set_signature_verification_result(SignatureVerificationResult signature_verification_result)
{
    m_signature_verification_result = signature_verification_result;
}

SignatureVerificationResult FileBase::get_signature_verification_result() const
{
    return m_signature_verification_result;
}

void FileBase::set_functor(const std::string &xpath, FunctorGen *gen)
{
    m_map_funcgens[xpath] = gen;
}

FunctorGen *FileBase::get_functor(const std::string &xpath)
{
    std::map<std::string, FunctorGen *>::iterator it;

    it = m_map_funcgens.find(xpath);
    if (it == m_map_funcgens.end()) return nullptr;
    return it->second;
}

std::shared_ptr<Data> FileBase::get_data_or_new()
{
    if (get_data() != nullptr) return get_data();

    std::shared_ptr<Data> data = std::make_shared<Data>();

    set_data(data);
    return data;
}

} // namespace xml

} // namespace esysfile
