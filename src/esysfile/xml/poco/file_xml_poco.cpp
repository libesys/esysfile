/*!
 * \file esysfile/xml/poco/file_xml_poco.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/poco/file.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/attr.h"

#include "esysfile/xml/poco/fileimpl.h"

#include <Poco/XML/XMLWriter.h>
#include <Poco/DOM/DOMWriter.h>
#include <Poco/XML/NamePool.h>
#include <Poco/DOM/AutoPtr.h>

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>

#include <cassert>

namespace esysfile::xml::poco
{

File::File(const std::string &filename)
    : FileBase(filename)
{
    m_impl = std::make_unique<FileImpl>(this);
}

File::~File()
{
}

FileImpl *File::get_impl()
{
    return m_impl.get();
}

int File::write(const std::string &filename)
{
    if (filename.empty() == false) set_file_name(filename);

    assert(m_impl != nullptr);

    return m_impl->write(get_file_name());
}

int File::write(std::ostream &output_stream)
{
    return m_impl->write(output_stream);
}

int File::read(const std::string &filename)
{
    if (filename.empty() == false) set_file_name(filename);

    boost::filesystem::path path = get_file_name();
    path = path.generic_string();
    if (path.is_absolute() == false) path = boost::filesystem::absolute(path);
    set_file_name(path.string());

    return m_impl->read(path);
}

int File::read(std::istream &input_stream)
{
    return m_impl->read(input_stream);
}

} // namespace esysfile::xml::poco
