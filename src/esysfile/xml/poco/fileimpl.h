/*!
 * \file esysfile/xml/poco/fileimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/element.h"

#include <Poco/DOM/Document.h>
#include <Poco/DOM/NodeIterator.h>
#include <Poco/DOM/AutoPtr.h> //typedef to Poco::AutoPtr
#include <Poco/DOM/DOMParser.h>
#include <Poco/DOM/NodeFilter.h>
#include <Poco/DOM/NamedNodeMap.h>
#include <Poco/SAX/InputSource.h>
#include <Poco/SAX/SAXException.h>

#include <boost/filesystem.hpp>

#include <string>

namespace esysfile::xml::poco
{

class ESYSFILE_API File;

/*! \class FileImpl esysfile/xml/poco/fileimpl.h "esysfile/xml/poco/fileimpl.h"
 * \brief PIMPL class for the implementation of File with POCO
 */
class ESYSFILE_API FileImpl
{
public:
    //! Constructor
    FileImpl(File *self = nullptr);

    //! Destructor
    ~FileImpl();

    /*! \brief Write the XML data to a file
     * \param[in] filename the filename
     * \return 0 if successful, < 0 otherwise
     */
    int write(const boost::filesystem::path &filename);

    /*! \brief Write the XML data to an output stream
     * \param[in] output_stream the stream
     * \return 0 if successful, < 0 otherwise
     */
    int write(std::ostream &output_stream);

    /*! \brief Read the XML data from a file
     * \param[in] filename the filename
     * \return 0 if successful, < 0 otherwise
     */
    int read(const boost::filesystem::path &filename);

    /*! \brief Read the XML data from a input stream
     * \param[in] input_stream the input stream
     * \return 0 if successful, < 0 otherwise
     */
    int read(std::istream &input_stream);

    /*! \brief Read all children of a given POCO XML Node
     * \param[in] node the POCO XML Node
     * \param[in] parent the parent XML element
     * \return 0 if successful, < 0 otherwise
     */
    int read_children(Poco::XML::Node *node, std::shared_ptr<Element> parent);

    /*! \brief Read all attribures of a given POCO XML Node
     * \param[in] node the POCO XML Node
     * \param[in] el the abstract XML element where the attributes will be added
     * \return 0 if successful, < 0 otherwise
     */
    int read_attributes(Poco::XML::Node *node, std::shared_ptr<Element> el);

    /*! \brief Write an abstract XML element as a child to the POCO XML Node
     * \param[in] parent the POCO XML Node
     * \param[in] child the child abstract XML element
     * \return 0 if successful, < 0 otherwise
     */
    int write(Poco::XML::Node *parent, const std::shared_ptr<Element> &child);

    /*! \brief Write all abstract XML children of a given POCO XML Node
     * \param[in] parent the POCO XML Node
     * \param[in] children the abstract XML children elements
     * \return 0 if successful, < 0 otherwise
     */
    int write_children(Poco::XML::Node *parent, const std::vector<std::shared_ptr<Element>> &children);

    /*! \brief Get a "backward" pointer to the main class
     * \return the main class
     */
    File *self();

private:
    //!< \cond DOXY_IMPL
    File *m_self = nullptr;                        //!< "backward" pointer to the main class
    Poco::XML::AutoPtr<Poco::XML::Document> m_doc; //!< The POCO XML document
    Poco::XML::AutoPtr<Poco::XML::Element> m_root; //!< The POCO XML root element
    Poco::XML::Node *m_node = nullptr;             //!< A POCO XML node
    Poco::XML::Node *m_child_node = nullptr;       //!< A POCO XML child node
    //!< \endcond
};

} // namespace esysfile::xml::poco
