/*!
 * \file esysfile/xml/poco/fileimpl_xml_poco.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/poco/fileimpl.h"
#include "esysfile/xml/poco/file.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/attr.h"

#include <Poco/XML/XMLWriter.h>
#include <Poco/DOM/DOMWriter.h>
#include <Poco/XML/NamePool.h>
#include <Poco/DOM/AutoPtr.h>
#include <Poco/DOM/Text.h>

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>

#include <memory>
#include <cassert>

namespace esysfile
{

namespace xml
{

namespace poco
{

FileImpl::FileImpl(File *file)
    : m_self(file)
{
}

FileImpl::~FileImpl()
{
}

File *FileImpl::self()
{
    return m_self;
}

int FileImpl::write(std::ostream &output_stream)
{
    std::string s;
    std::string root_name;
    int result;
    std::shared_ptr<Attr> attr;
    std::size_t idx;

    assert(self() != nullptr);

    if (self()->get_data() == nullptr) return -1;

    auto data = self()->get_data();

    root_name = data->get_root_node_name();

    m_doc = new Poco::XML::Document;
    m_root = m_doc->createElement(root_name);

    if (!data->get_root_attr_name().empty()) m_root->setAttribute("Name", data->get_root_attr_name());
    if (!data->get_root_attr_version().empty()) m_root->setAttribute("Version", data->get_root_attr_version());

    for (idx = 0; idx < data->get_attr_count(); ++idx)
    {
        attr = data->get_attr(idx);
        m_root->setAttribute(attr->get_name(), attr->get_value());
    }

    m_doc->appendChild(m_root);

    result = write_children(m_root, data->get_elements());
    if (result < 0) return result;

    Poco::XML::DOMWriter writer;
    writer.setNewLine("\n");
    // writer.setOptions(Poco::XML::XMLWriter::PRETTY_PRINT);
    if (!self()->get_indent().empty())
    {
        writer.setOptions(Poco::XML::XMLWriter::PRETTY_PRINT | Poco::XML::XMLWriter::WRITE_XML_DECLARATION);
        writer.setIndent(self()->get_indent());
    }
    else
        writer.setOptions(Poco::XML::XMLWriter::WRITE_XML_DECLARATION);

    writer.writeNode(output_stream, m_doc);

    return 0;
}

int FileImpl::write(const boost::filesystem::path &filename)
{
    std::ofstream ofs;

    ofs.open(filename.string().c_str());
    if (!ofs.is_open()) return -1;

    auto result = write(ofs);
    ofs.close();

    return result;
}

int FileImpl::read(std::istream &input_stream)
{
    Poco::XML::InputSource src(input_stream);
    // Poco::XML::NamePool *name_pool = new Poco::XML::NamePool((unsigned long)4093);
    Poco::AutoPtr<Poco::XML::NamePool> name_pool = new Poco::XML::NamePool((unsigned long)4093);
    Poco::XML::DOMParser parser(name_pool);

    // src.setCharacterStream(*ifs);
#if !defined __GNUC__ || !defined __GNUG__
    // parser.setFeature(Poco::XML::DOMParser::FEATURE_FILTER_WHITESPACE, true);
#endif

    parser.setFeature(Poco::XML::XMLReader::FEATURE_NAMESPACE_PREFIXES, true);

    try
    {
        m_doc = parser.parse(&src);
    }
    catch (Poco::XML::SAXParseException &e)
    {
        std::cout << e.displayText() << std::endl;
        return -1;
    }

    std::string s;
    int result;
    Poco::XML::Element *root_element = m_doc->documentElement();
    std::string root_name;
    std::string root_prefix;

    if (root_element == nullptr) return -1;

    root_name = root_element->localName();
    root_prefix = root_element->prefix();

    if (self()->get_data() == nullptr) return -1;

    auto data = self()->get_data();

    if (root_name != data->get_root_node_name()) return -1;
    if (root_prefix != data->get_root_node_prefix()) return -1;

    data->set_name(root_name);
    read_attributes(root_element, data);

    result = read_children(root_element, data);
    return result;
}

int FileImpl::read(const boost::filesystem::path &filename)
{
    std::unique_ptr<std::ifstream> ifs;
    int result;
    boost::filesystem::path path;

    if (self()->get_data() == nullptr) return -1;

    path = filename;
    ifs = std::make_unique<std::ifstream>(path.make_preferred().c_str(), std::ios::binary);
    if (ifs == nullptr) return -1;
    if (ifs->is_open() == false) return -2;

    result = read(*ifs);

    ifs->close();
    return result;
}

int FileImpl::write(Poco::XML::Node *parent, const std::shared_ptr<Element> &child_el)
{
    Poco::XML::AutoPtr<Poco::XML::Element> p_el = m_doc->createElement(child_el->get_name());
    Poco::XML::AutoPtr<Poco::XML::Text> pel_txt;
    // AutoPtr<Text> pText1 = pDoc->createTextNode("text1")

    std::shared_ptr<Attr> attr;
    std::size_t idx;
    // Add the attributes, text, etc

    if (child_el->has_value())
    {
        pel_txt = m_doc->createTextNode(child_el->get_value());
        p_el->appendChild(pel_txt);
    }
    else
    {
    }

    for (idx = 0; idx < child_el->get_attr_count(); ++idx)
    {
        attr = child_el->get_attr(idx);
        p_el->setAttribute(attr->get_name(), attr->get_value());
    }

    parent->appendChild(p_el);

    return write_children(p_el, child_el->get_elements());
}

int FileImpl::write_children(Poco::XML::Node *parent, const std::vector<std::shared_ptr<Element>> &children)
{
    int result = 0;

    for (auto child_el : children)
    {
        result = write(parent, child_el);
        if (result < 0) return result;
    }

    return 0;
}

int FileImpl::read_children(Poco::XML::Node *parent_node, std::shared_ptr<Element> parent)
{
    std::shared_ptr<Element> xml_el;
    std::string s;
    std::string node_name;
    std::string parent_node_name;
    int result;

    parent_node_name = parent_node->nodeName();

    auto child_node = parent_node->firstChild();

    while (child_node)
    {
        node_name = child_node->nodeName();

        if (child_node->nodeType() == Poco::XML::Node::TEXT_NODE)
        {
            std::string str = child_node->getNodeValue();
            parent->set_value(str);
        }
        else
        {
            xml_el = std::make_shared<Element>();
            xml_el->set_name(node_name);
            parent->add_element(xml_el);

            if (child_node->hasAttributes())
            {
                read_attributes(child_node, xml_el);
            }
            if (child_node->hasChildNodes() == true)
            {
                result = read_children(child_node, xml_el);
                if (result < 0) return result;
            }
        }
        child_node = child_node->nextSibling();
    }
    return 0;
}

/*int FileImpl::read_attributes(Poco::XML::Node *node)
{
    assert(m_el != nullptr);

    return read_attributes(node, m_el);
} */

int FileImpl::read_attributes(Poco::XML::Node *node, std::shared_ptr<Element> el)
{
    if (!node->hasAttributes()) return -1;

    Poco::XML::NamedNodeMap *attrs;
    Poco::XML::Node *attr;
    std::shared_ptr<Attr> xml_attr;
    std::size_t i;

    attrs = node->attributes();

    for (i = 0; i < attrs->length(); ++i)
    {
        attr = attrs->item((unsigned long)i);
        xml_attr = std::make_shared<Attr>();

        // xml_attr->set_name(attr->localName().c_str());
        xml_attr->set_name(attr->nodeName().c_str());

        xml_attr->set_value(attr->nodeValue());
        el->add_attr(xml_attr);
    }
    attrs->release();

    return 0;
}

} // namespace poco

} // namespace xml

} // namespace esysfile
