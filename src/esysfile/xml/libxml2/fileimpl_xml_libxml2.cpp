/*!
 * \file esysfile/xml/libxml2/fileimpl_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/libxml2/fileimpl.h"
#include "esysfile/xml/libxml2/file.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/attr.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include <xmlsec/xmlsec.h>
#include <xmlsec/xmltree.h>
#include <xmlsec/xmldsig.h>
#include <xmlsec/templates.h>
#include <xmlsec/crypto.h>

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <memory>
#include <cassert>

namespace esysfile::xml::libxml2
{

std::unique_ptr<LibInit> FileImpl::s_libxml2_init;

void FileImpl::init_library()
{
    if (s_libxml2_init == nullptr) s_libxml2_init = std::make_unique<LibInit>();
}

const xmlChar *FileImpl::to_xml_char(const std::string &s)
{
    return (const xmlChar *)s.c_str();
}

FileImpl::FileImpl(File *file)
    : m_self(file)
{
    init_library();
}

FileImpl::~FileImpl()
{
}

File *FileImpl::self()
{
    return m_self;
}

int FileImpl::write(const boost::filesystem::path &filename)
{
    assert(self() != nullptr);

    if (self()->get_data() == nullptr) return -1;

    auto data = self()->get_data();

    // Create a new XmlWriter for DOM, with no compression.
    m_writer = xmlNewTextWriterDoc(m_doc.get_p(), 0);
    if (m_writer == nullptr) return -1;

    if (!self()->get_indent().empty())
    {
        int result = xmlTextWriterSetIndent(m_writer.get(), 1);
        if (result < 0) return result;

        result = xmlTextWriterSetIndentString(m_writer.get(), to_xml_char(self()->get_indent()));
        if (result < 0) return result;
    }

    // Start the document with the xml default for the version,
    // encoding ISO 8859-1 and the default for the standalone
    // declaration.
    int result = xmlTextWriterStartDocument(m_writer.get(), nullptr, get_encoding().c_str(), nullptr);
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterStartDocument\n");
        return result;
    }

    // Start an element named "EXAMPLE". Since this is the first
    // element, this will be the root element of the document.
    result = xmlTextWriterStartElement(m_writer.get(), to_xml_char(self()->get_root_node_fullname()));
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterStartElement\n");
        return result;
    }

    if (!data->get_root_attr_name().empty())
    {
        result =
            xmlTextWriterWriteAttribute(m_writer.get(), to_xml_char("name"), to_xml_char(data->get_root_attr_name()));
        if (result < 0) return result;
    }
    if (!data->get_root_attr_version().empty())
    {
        result = xmlTextWriterWriteAttribute(m_writer.get(), to_xml_char("version"),
                                             to_xml_char(data->get_root_attr_version()));

        if (result < 0) return result;
    }

    for (std::size_t idx = 0; idx < data->get_attr_count(); ++idx)
    {
        auto attr = data->get_attr(idx);
        result =
            xmlTextWriterWriteAttribute(m_writer.get(), to_xml_char(attr->get_name()), to_xml_char(attr->get_value()));

        if (result < 0) return result;
    }

    result = write_children(data->get_elements());
    if (result < 0) return result;

    // Close the root element
    result = xmlTextWriterEndElement(m_writer.get());
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterEndElement\n");
        return result;
    }

    // Here we could close the elements ORDER and EXAMPLE using the
    // function xmlTextWriterEndElement, but since we do not want to
    // write any other elements, we simply call xmlTextWriterEndDocument,
    // which will do all the work.
    result = xmlTextWriterEndDocument(m_writer.get());
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterEndDocument\n");
        return result;
    }

    m_writer.reset();

    if (self()->get_digitally_signed())
    {
        result = add_signature();
        if (result < 0) return result;
    }

    result = xmlSaveFileEnc(self()->get_file_name().c_str(), m_doc.get(), get_encoding().c_str());
    if (result < 0) return result;
    return 0;
}

int FileImpl::write(std::ostream &output_stream)
{
    return -1;
}

int FileImpl::read(const boost::filesystem::path &filename)
{
    auto data = self()->get_data();

    if (data == nullptr) return -1;

    m_doc = xmlParseFile(filename.string().c_str());
    if ((m_doc == nullptr) || (xmlDocGetRootElement(m_doc.get()) == nullptr))
    {
        return -1;
    }

    int result = 0;
    xmlNodePtr root_ptr = xmlDocGetRootElement(m_doc.get());
    std::string root_name = (char *)root_ptr->name;
    std::string root_namespace;

    if (root_ptr->ns != nullptr) root_namespace = (char *)root_ptr->ns->prefix;

    if (root_name != self()->get_root_node_name()) return -1;
    if (root_namespace != self()->get_root_node_prefix()) return -1;

    // find signature start node
    xmlNodePtr sign_node = xmlSecFindNode(xmlDocGetRootElement(m_doc.get()), xmlSecNodeSignature, xmlSecDSigNs);
    if (sign_node != nullptr)
    {
        // The file does have a signature
        self()->set_digitally_signed(true);

        create_key_manager();
        verify_signature(sign_node);
    }

    data->set_name(root_name);
    result = read_attributes(root_ptr, data);
    if (result < 0) return result;

    result = read_children(root_ptr, data);
    return result;
}

void FileImpl::verify_signature(xmlNodePtr sign_node)
{
    // create signature context
    m_dsig_ctx = xmlSecDSigCtxCreate(m_key_mngr.get());
    if (m_dsig_ctx == nullptr)
    {
        // fprintf(stderr, "Error: failed to create signature context\n");
        self()->set_signature_verification_result(SignatureVerificationResult::VERIFICATION_FAILED);
        return;
    }

    // Verify signature
    if (xmlSecDSigCtxVerify(m_dsig_ctx.get(), sign_node) < 0)
    {
        // fprintf(stderr, "Error: signature verify\n");
        self()->set_signature_verification_result(SignatureVerificationResult::VERIFICATION_FAILED);
        return;
    }

    // Set the result of the signature verification
    if (m_dsig_ctx.get()->status == xmlSecDSigStatusSucceeded)
    {
        self()->set_signature_verification_result(SignatureVerificationResult::VALID_SIGNATURE);
    }
    else
    {
        self()->set_signature_verification_result(SignatureVerificationResult::SIGNATURE_DATA_NO_MATCH);
    }
}

void FileImpl::create_key_manager()
{
    m_key_mngr = xmlSecKeysMngrCreate();
    if (m_key_mngr == nullptr)
    {
        // fprintf(stderr, "Error: failed to create keys manager.\n");
        return;
    }
    if (xmlSecCryptoAppDefaultKeysMngrInit(m_key_mngr.get()) < 0)
    {
        // fprintf(stderr, "Error: failed to initialize keys manager.\n");
        xmlSecKeysMngrDestroy(m_key_mngr.get());
        m_key_mngr = nullptr;
    }

    for (auto &trusted_cert_file : self()->get_trusted_cert_files())
    {
        if (xmlSecCryptoAppKeysMngrCertLoad(m_key_mngr.get(), trusted_cert_file.c_str(), xmlSecKeyDataFormatPem,
                                            xmlSecKeyDataTypeTrusted)
            < 0)
        {
            // fprintf(stderr, "Error: failed to load pem certificate from \"%s\"\n", files[i]);
            xmlSecKeysMngrDestroy(m_key_mngr.get());
            m_key_mngr = nullptr;
            return;
        }
    }
}

int FileImpl::read_children(xmlNodePtr node, std::shared_ptr<Element> parent)
{
    if (node->children == nullptr) return 0;

    std::shared_ptr<Element> xml_el;

    auto cur_node = node->children;
    int children_count = 0;

    while (cur_node != nullptr)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
            xml_el = std::make_shared<Element>();
            std::string name = (char *)(cur_node->name);
            xml_el->set_name(name);
            xml_el->set_line_number(cur_node->line);
            parent->add_element(xml_el);

            if (cur_node->properties != nullptr)
            {
                int result = read_attributes(cur_node, xml_el);
                if (result < 0) return result;
            }
            if (cur_node->children != nullptr)
            {
                int result = read_children(cur_node, xml_el);
                if (result < 0) return result;
            }
        }
        else if (cur_node->type == XML_TEXT_NODE)
        {
            bool do_something = false;
            std::string content = (char *)cur_node->content;
            std::string content_trim = boost::algorithm::trim_copy(content);

            if ((cur_node->prev == nullptr) && (cur_node->next == nullptr)) do_something = true;

            if (do_something)
            {
                if (children_count == 0) parent->set_value(content);
            }
        }
        cur_node = cur_node->next;
        ++children_count;
    }
    return 0;
}

int FileImpl::read_attribute(xmlAttrPtr attr, std::shared_ptr<Element> el)
{
    std::string name = (char *)attr->name;
    std::string value;

    if (attr->children == nullptr) return -1;
    if (attr->children->type != XML_TEXT_NODE) return -1;

    value = (char *)attr->children->content;
    el->add_attr(name, value);
    return 0;
}

int FileImpl::read_attributes(xmlNodePtr node, std::shared_ptr<Element> el)
{
    if (node->properties == nullptr) return 0;

    auto cur_prop = node->properties;

    while (cur_prop != nullptr)
    {
        int result = read_attribute(cur_prop, el);
        if (result < 0) return result;

        cur_prop = cur_prop->next;
    }
    return 0;
}

int FileImpl::write(const std::shared_ptr<Element> &child)
{
    int result = xmlTextWriterStartElement(m_writer.get(), to_xml_char(child->get_name()));
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterStartElement\n");
        return result;
    }

    if (child->has_value())
    {
        result = xmlTextWriterWriteString(m_writer.get(), to_xml_char(child->get_value()));
        if (result < 0) return result;
    }
    else
    {
    }

    for (std::size_t idx = 0; idx < child->get_attr_count(); ++idx)
    {
        auto attr = child->get_attr(idx);

        // Add an attribute
        result =
            xmlTextWriterWriteAttribute(m_writer.get(), to_xml_char(attr->get_name()), to_xml_char(attr->get_value()));
        if (result < 0)
        {
            // printf("testXmlwriterDoc: Error at xmlTextWriterWriteAttribute\n");
            return result;
        }
    }

    result = write_children(child->get_elements());
    if (result < 0) return result;

    // Close the element
    result = xmlTextWriterEndElement(m_writer.get());
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterEndElement\n");
        return result;
    }
    return 0;
}

int FileImpl::write_children(const std::vector<std::shared_ptr<Element>> &children)
{
    int result = 0;

    for (auto child_el : children)
    {
        result = write(child_el);
        if (result < 0) return result;
    }

    return 0;
}

void FileImpl::set_encoding(const std::string &encoding)
{
    m_encoding = encoding;
}

const std::string &FileImpl::get_encoding() const
{
    return m_encoding;
}

int FileImpl::add_signature()
{
    // create signature template for RSA-SHA1 enveloped signature
    xmlNodePtr sign_node =
        xmlSecTmplSignatureCreate(m_doc.get(), xmlSecTransformExclC14NId, xmlSecTransformRsaSha512Id, nullptr);
    if (sign_node == nullptr)
    {
        // fprintf(stderr, "Error: failed to create signature template\n");
        return -1;
    }

    // add <dsig:Signature/> node to the doc
    xmlAddChild(xmlDocGetRootElement(m_doc.get()), sign_node);

    // add reference
    xmlNodePtr ref_node = xmlSecTmplSignatureAddReference(sign_node, xmlSecTransformSha512Id, NULL, NULL, NULL);
    if (ref_node == nullptr)
    {
        // fprintf(stderr, "Error: failed to add reference to signature template\n");
        return -2;
    }

    // add enveloped transform
    if (xmlSecTmplReferenceAddTransform(ref_node, xmlSecTransformEnvelopedId) == nullptr)
    {
        // fprintf(stderr, "Error: failed to add enveloped transform to reference\n");
        return -3;
    }

    // add <dsig:KeyInfo/> and <dsig:X509Data/>
    xmlNodePtr key_info_node = xmlSecTmplSignatureEnsureKeyInfo(sign_node, nullptr);
    if (key_info_node == nullptr)
    {
        // fprintf(stderr, "Error: failed to add key info\n");
        return -4;
    }

    xmlNodePtr x509_data_node = xmlSecTmplKeyInfoAddX509Data(key_info_node);
    if (x509_data_node == nullptr)
    {
        // fprintf(stderr, "Error: failed to add X509Data node\n");
        return -5;
    }

    if (xmlSecTmplX509DataAddSubjectName(x509_data_node) == nullptr)
    {
        // fprintf(stderr, "Error: failed to add X509SubjectName node\n");
        return -6;
    }

    if (xmlSecTmplX509DataAddCertificate(x509_data_node) == nullptr)
    {
        // fprintf(stderr, "Error: failed to add X509Certificate node\n");
        return -7;
    }

    // create signature context, we don't need keys manager in this example
    m_dsig_ctx = xmlSecDSigCtxCreate(nullptr);
    if (m_dsig_ctx == nullptr)
    {
        // fprintf(stderr, "Error: failed to create signature context\n");
        return -8;
    }

    // load private key, assuming that there is not password
    m_dsig_ctx.get()->signKey =
        xmlSecCryptoAppKeyLoad(self()->get_key_file().c_str(), xmlSecKeyDataFormatPem, nullptr, nullptr, nullptr);
    if (m_dsig_ctx.get()->signKey == nullptr)
    {
        // fprintf(stderr, "Error: failed to load private pem key from \"%s\"\n", key_file);
        return -9;
    }

    // load certificate and add to the key
    if (xmlSecCryptoAppKeyCertLoad(m_dsig_ctx.get()->signKey, self()->get_cert_file().c_str(), xmlSecKeyDataFormatPem)
        < 0)
    {
        // fprintf(stderr, "Error: failed to load pem certificate \"%s\"\n", cert_file);
        return -10;
    }

    // set key name to the file name, this is just an example! */
    if (xmlSecKeySetName(m_dsig_ctx.get()->signKey, to_xml_char(self()->get_key_file())) < 0)
    {
        // fprintf(stderr, "Error: failed to set key name for key from \"%s\"\n", key_file);
        return -11;
    }

    // sign the template
    if (xmlSecDSigCtxSign(m_dsig_ctx.get(), sign_node) < 0)
    {
        // fprintf(stderr, "Error: signature failed\n");
        return -12;
    }

    return 0;
}

} // namespace esysfile::xml::libxml2
