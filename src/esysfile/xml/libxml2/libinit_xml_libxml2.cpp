/*!
 * \file esysfile/xml/libxml2/libinit_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/libxml2/libinit.h"

#include <xmlsec/xmlsec.h>
#include <xmlsec/crypto.h>

#include <libxml/tree.h>

namespace esysfile
{

namespace xml
{

namespace libxml2
{

LibInit::LibInit()
{
    xmlInitParser();

    m_result[0] = xmlSecInit();
    if (m_result[0] < 0) return;

    m_result[1] = xmlSecCryptoAppInit(nullptr);
    if (m_result[1] < 0) return;

    m_result[2] = xmlSecCryptoInit();
}

LibInit::~LibInit()
{
#ifdef WIN32
    // For some strange reason, this core dump in Linux
    if (m_result[2] == 0) xmlSecCryptoShutdown();
#endif
    if (m_result[1] == 0) xmlSecCryptoAppShutdown();
    if (m_result[0] == 0) xmlSecShutdown();

    xmlCleanupParser();
}

} // namespace libxml2

} // namespace xml

} // namespace esysfile
