/*!
 * \file esysfile/xml/libxml2/libinit.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

namespace esysfile
{

namespace xml
{

namespace libxml2
{

/*! \class LibInit esysfile/xml/libxml2/libinit.h "esysfile/xml/libxml2/libinit.h"
 * \brief Class used to initialize and release the libxml2
 */
class ESYSFILE_API LibInit
{
public:
    //! Constructor
    LibInit();

    //! Destructor
    ~LibInit();

private:
    int m_result[3] = {-1, -1, -1};
};

} // namespace libxml2

} // namespace xml

} // namespace esysfile
