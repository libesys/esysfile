/*!
 * \file esysfile/xml/libxml2/guardassign.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#include <libxml/parser.h>

#include <memory>
#include <cassert>

namespace esysfile
{

namespace xml
{

namespace libxml2
{

template<typename T>
class Guard;

template<typename T>
int guard_assign(Guard<T> &dest, const Guard<T> &src)
{
    assert(false);

    return -1;
}

template<>
ESYSFILE_API int guard_assign<xmlDoc>(Guard<xmlDoc> &dest, const Guard<xmlDoc> &src);

} // namespace libxml2

} // namespace xml

} // namespace esysfile
