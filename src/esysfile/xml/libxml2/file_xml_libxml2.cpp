/*!
 * \file esysfile/xml/libxml2/file_xml_poco.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c)2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/libxml2/file.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/attr.h"

#include "esysfile/xml/libxml2/fileimpl.h"

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>

#include <cassert>

namespace esysfile
{

namespace xml
{

namespace libxml2
{

File::File(const std::string &filename)
{
    m_impl = std::make_unique<FileImpl>(this);
}

File::~File()
{
}

FileImpl *File::get_impl()
{
    return m_impl.get();
}

int File::write(const std::string &filename)
{
    if (filename.empty() == false) set_file_name(filename);

    assert(get_impl() != nullptr);

    return get_impl()->write(get_file_name());
}

int File::write(std::ostream &output_stream)
{
    return get_impl()->write(output_stream);
}

int File::read(const std::string &filename)
{
    set_signature_verification_result(SignatureVerificationResult::NO_SIGNATURE);
    set_digitally_signed(false);

    if (filename.empty() == false) set_file_name(filename);

    boost::filesystem::path path = get_file_name();
    path = path.generic_string();
    if (path.is_absolute() == false) path = boost::filesystem::absolute(path);
    set_file_name(path.string());

    return get_impl()->read(path);
}

int File::read(std::istream &input_stream)
{
    return -1;
    // return m_impl->read(input_stream);
}

bool File::get_signature_support() const
{
    return true;
}

/*bool File::check_signature()
{
    return get_impl()->check_signature();
}

int File::add_signature()
{
    return get_impl()->add_signature();
}*/

} // namespace libxml2

} // namespace xml

} // namespace esysfile
