/*!
 * \file esysfile/xml/libxml2/guardrelease.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#include <libxml/parser.h>
#include <libxml/xmlwriter.h>

#include <xmlsec/xmlsec.h>

#include <memory>
#include <cassert>

namespace esysfile
{

namespace xml
{

namespace libxml2
{

template<typename T>
void guard_release(T *data)
{
    assert(false);
}

template<>
ESYSFILE_API void guard_release<xmlDoc>(xmlDoc *data);

template<>
ESYSFILE_API void guard_release<xmlTextWriter>(xmlTextWriter *data);

template<>
ESYSFILE_API void guard_release<xmlSecDSigCtx>(xmlSecDSigCtx *data);

template<>
ESYSFILE_API void guard_release<xmlSecKeysMngr>(xmlSecKeysMngr *data);

} // namespace libxml2

} // namespace xml

} // namespace esysfile
