/*!
 * \file esysfile/xml/libxml2/guard.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/libxml2/guardrelease.h"
#include "esysfile/xml/libxml2/guardassign.h"

namespace esysfile
{

namespace xml
{

namespace libxml2
{

/*! \class Guard esysfile/xml/libxml2/guard.h "esysfile/xml/libxml2/guard.h"
 * \brief Guard for a object which must be release in a custom way
 */
template<typename T>
class Guard
{
public:
    template<typename U>
    friend int guard_assign(Guard<U> &dest, const Guard<U> &src);

    //! Default constructor
    Guard();

    //! Constructor
    Guard(T *data);

    //! Destructor
    ~Guard();

    T *get() const;
    T **get_p();

    void reset();

    //! Assignement operator
    void operator=(const Guard &other);

    //! Assignement operator
    void operator=(T *other);

    //! Equal to comparison operator
    bool operator==(const T *other) const;

    //! Not equal to comparison operator
    bool operator!=(const T *other) const;

private:
    //!< \cond DOXY_IMPL
    T *m_data = nullptr;
    //!< \endcond
};

template<typename T>
Guard<T>::Guard()
{
}

template<typename T>
Guard<T>::Guard(T *data)
    : m_data(data)
{
}

template<typename T>
Guard<T>::~Guard()
{
    guard_release<T>(m_data);
}

template<typename T>
T *Guard<T>::get() const
{
    return m_data;
}

template<typename T>
T **Guard<T>::get_p()
{
    return &m_data;
}

template<typename T>
void Guard<T>::reset()
{
    if (m_data == nullptr) return;

    guard_release<T>(m_data);
    m_data = nullptr;
}

template<typename T>
void Guard<T>::operator=(const Guard &other)
{
    int result = guard_assign<T>(*this, other);
    // \TODO throw exception??
}

template<typename T>
void Guard<T>::operator=(T *other)
{
    m_data = other;
}

template<typename T>
bool Guard<T>::operator==(const T *other) const
{
    return (m_data == other);
}

template<typename T>
bool Guard<T>::operator!=(const T *other) const
{
    return (m_data != other);
}

} // namespace libxml2

} // namespace xml

} // namespace esysfile
