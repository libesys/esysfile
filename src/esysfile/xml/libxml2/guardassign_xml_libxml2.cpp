/*!
 * \file esysfile/xml/libxml2/guardassign_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/libxml2/guardassign.h"
#include "esysfile/xml/libxml2/guard.h"

namespace esysfile
{

namespace xml
{

namespace libxml2
{

template<>
ESYSFILE_API int guard_assign<xmlDoc>(Guard<xmlDoc> &dest, const Guard<xmlDoc> &src)
{
    // If a previous object is guarded, release it
    dest.reset();

    dest = src.get();

    return 0;
}

} // namespace libxml2

} // namespace xml

} // namespace esysfile
