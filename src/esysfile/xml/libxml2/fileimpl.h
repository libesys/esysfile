/*!
 * \file esysfile/xml/libxml2/fileimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/libxml2/libinit.h"
#include "esysfile/xml/libxml2/guard.h"

#include <libxml/tree.h>
#include <libxml/xmlwriter.h>

#include <xmlsec/xmlsec.h>

#include <boost/filesystem.hpp>

#include <string>

namespace esysfile::xml::libxml2
{

class ESYSFILE_API File;

/*! \class FileImpl esysfile/xml/libxml2/fileimpl.h "esysfile/xml/libxml2/fileimpl.h"
 * \brief
 */
class ESYSFILE_API FileImpl
{
public:
    //! Constructor
    FileImpl(File *self = nullptr);

    //! Destructor
    ~FileImpl();

    /*! \brief Write the XML data to a file
     * \param[in] filename the filename
     * \return 0 if successful, < 0 otherwise
     */
    int write(const boost::filesystem::path &filename);

    /*! \brief Write the XML data to an output stream
     * \param[in] output_stream the stream
     * \return 0 if successful, < 0 otherwise
     */
    int write(std::ostream &output_stream);

    /*! \brief Read the XML data from a file
     * \param[in] filename the filename
     * \return 0 if successful, < 0 otherwise
     */
    int read(const boost::filesystem::path &filename);
    // virtual int read(std::istream &input_stream);

    /*! \brief Read all children of a given libxml2 XML node
     * \param[in] node the libxml2 XML node
     * \param[in] parent the parent XML element
     * \return 0 if successful, < 0 otherwise
     */
    int read_children(xmlNodePtr node, std::shared_ptr<Element> parent);

    /*! \brief Read all attribures of a given libxml2 XML Node
     * \param[in] node the libxml2 XML Node
     * \param[in] el the abstract XML element where the attributes will be added
     * \return 0 if successful, < 0 otherwise
     */
    int read_attributes(xmlNodePtr node, std::shared_ptr<Element> el);
    int read_attribute(xmlAttrPtr attr, std::shared_ptr<Element> el);

    int write(const std::shared_ptr<Element> &child);
    int write_children(const std::vector<std::shared_ptr<Element>> &children);

    void set_encoding(const std::string &encoding);
    const std::string &get_encoding() const;

    int add_signature();

    /*! \brief Get a "backward" pointer to the main class
     * \return the main class
     */
    File *self();

private:
    //!< \cond DOXY_IMPL
    static void init_library();
    static const xmlChar *to_xml_char(const std::string &s);

    static std::unique_ptr<LibInit> s_libxml2_init;

    void verify_signature(xmlNodePtr sign_node);
    void create_key_manager();

    File *m_self = nullptr;

    Guard<xmlTextWriter> m_writer;
    Guard<xmlDoc> m_doc;
    Guard<xmlSecDSigCtx> m_dsig_ctx;
    Guard<xmlSecKeysMngr> m_key_mngr;
    std::string m_encoding = "ISO-8859-1";
    //!< \endcond
};

} // namespace esysfile::xml::libxml2
