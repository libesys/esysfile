/*!
 * \file esysfile/xml/libxml2/guardrelease_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/libxml2/guardrelease.h"

#include <xmlsec/xmldsig.h>

namespace esysfile
{

namespace xml
{

namespace libxml2
{

template<>
ESYSFILE_API void guard_release<xmlDoc>(xmlDoc *data)
{
    if (data == nullptr) return;

    xmlFreeDoc(data);
}

template<>
ESYSFILE_API void guard_release<xmlTextWriter>(xmlTextWriter *data)
{
    if (data == nullptr) return;

    xmlFreeTextWriter(data);
}

template<>
ESYSFILE_API void guard_release<xmlSecDSigCtx>(xmlSecDSigCtx *data)
{
    if (data == nullptr) return;

    xmlSecDSigCtxDestroy(data);
}

template<>
ESYSFILE_API void guard_release<xmlSecKeysMngr>(xmlSecKeysMngr *data)
{
    if (data == nullptr) return;

    xmlSecKeysMngrDestroy(data);
}

} // namespace libxml2

} // namespace xml

} // namespace esysfile
