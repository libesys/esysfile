﻿/*!
 * \file esysfile/xml/libxml2/fileimpl_xml_libxml2.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/libxml2/fileimpl.h"
#include "esysfile/xml/libxml2/file.h"
#include "esysfile/xml/element.h"
#include "esysfile/xml/attr.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <memory>
#include <cassert>

namespace esysfile
{

namespace xml
{

namespace libxml2
{

std::unique_ptr<LibInit> FileImpl::s_libxml2_init;

void FileImpl::init_library()
{
    if (s_libxml2_init != nullptr) return;

    s_libxml2_init = std::make_unique<LibInit>();
}

const xmlChar *FileImpl::to_xml_char(const std::string &s)
{
    return (const xmlChar *)s.c_str();
}

FileImpl::FileImpl(File *file)
    : m_self(file)
{
    init_library();
}

FileImpl::~FileImpl()
{
}

File *FileImpl::self()
{
    return m_self;
}

int FileImpl::write(const boost::filesystem::path &filename)
{
    assert(self() != nullptr);

    if (self()->get_data() == nullptr) return -1;

    auto data = self()->get_data();

    // Create a new XmlWriter for DOM, with no compression.
    m_writer = xmlNewTextWriterDoc(m_doc.get_p(), 0);
    if (m_writer == nullptr) return -1;

    // Start the document with the xml default for the version,
    // encoding ISO 8859-1 and the default for the standalone
    // declaration.
    int result = xmlTextWriterStartDocument(m_writer.get(), nullptr, get_encoding().c_str(), nullptr);
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterStartDocument\n");
        return result;
    }

    // Start an element named "EXAMPLE". Since this is the first
    // element, this will be the root element of the document. */
    result = xmlTextWriterStartElement(m_writer.get(), to_xml_char(self()->get_root_node_fullname()));
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterStartElement\n");
        return result;
    }

    if (!data->get_root_attr_name().empty())
    {
        m_root->setAttribute("Name", data->get_root_attr_name());
    }
    if (!data->get_root_attr_version().empty())
    {
        m_root->setAttribute("Version", data->get_root_attr_version());
    }

    // Close the root element
    result = xmlTextWriterEndElement(m_writer);
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterEndElement\n");
        return result;
    }

    // Here we could close the elements ORDER and EXAMPLE using the
    // function xmlTextWriterEndElement, but since we do not want to
    // write any other elements, we simply call xmlTextWriterEndDocument,
    // which will do all the work.
    result = xmlTextWriterEndDocument(m_writer.get());
    if (result < 0)
    {
        // printf("testXmlwriterDoc: Error at xmlTextWriterEndDocument\n");
        return result;
    }

    return xmlSaveFileEnc(self()->get_file_name().c_str(), m_doc.get(), get_encoding().c_str());
}

int FileImpl::read(const boost::filesystem::path &filename)
{
    auto data = self()->get_data();

    if (data == nullptr) return -1;

    m_doc = xmlParseFile(filename.string().c_str());
    if ((m_doc == nullptr) || (xmlDocGetRootElement(m_doc.get()) == nullptr))
    {
        return -1;
    }

    int result = 0;
    xmlNodePtr root_ptr = xmlDocGetRootElement(m_doc.get());
    std::string root_name = (char *)root_ptr->name;
    std::string root_namespace;

    if (root_ptr->ns != nullptr) root_namespace = (char *)root_ptr->ns->prefix;

    if (root_name != self()->get_root_node_name()) return -1;
    if (root_namespace != self()->get_root_node_prefix()) return -1;

    data->set_name(root_name);
    result = read_attributes(root_ptr, data);
    if (result < 0) return result;

    result = read_children(root_ptr, data);
    return result;
}

int FileImpl::read_children(xmlNodePtr node, std::shared_ptr<Element> parent)
{
    if (node->children == nullptr) return 0;

    std::shared_ptr<Element> xml_el;

    auto cur_node = node->children;
    int children_count = 0;

    while (cur_node != nullptr)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
            xml_el = std::make_shared<Element>();
            std::string name = (char *)(cur_node->name);
            xml_el->set_name(name);
            xml_el->set_line_number(cur_node->line);
            parent->add_element(xml_el);

            if (cur_node->properties != nullptr)
            {
                int result = read_attributes(cur_node, xml_el);
                if (result < 0) return result;
            }
            if (cur_node->children != nullptr)
            {
                int result = read_children(cur_node, xml_el);
                if (result < 0) return result;
            }
        }
        else if (cur_node->type == XML_TEXT_NODE)
        {
            bool do_something = false;
            std::string content = (char *)cur_node->content;
            std::string content_trim = boost::algorithm::trim_copy(content);

            if ((cur_node->prev == nullptr) && (cur_node->next == nullptr)) do_something = true;

            if (do_something)
            {
                if (children_count == 0) parent->set_value(content);
            }
        }
        cur_node = cur_node->next;
        ++children_count;
    }
    return 0;
}

int FileImpl::read_attribute(xmlAttrPtr attr, std::shared_ptr<Element> el)
{
    std::string name = (char *)attr->name;
    std::string value;

    if (attr->children == nullptr) return -1;
    if (attr->children->type != XML_TEXT_NODE) return -1;

    value = (char *)attr->children->content;
    el->add_attr(name, value);
    return 0;
}

int FileImpl::read_attributes(xmlNodePtr node, std::shared_ptr<Element> el)
{
    if (node->properties == nullptr) return 0;

    auto cur_prop = node->properties;

    while (cur_prop != nullptr)
    {
        int result = read_attribute(cur_prop, el);
        if (result < 0) return result;

        cur_prop = cur_prop->next;
    }
    return 0;
}

int FileImpl::write(xmlNodePtr parent, const std::shared_ptr<Element> &child)
{
    return -1;
}

int FileImpl::write_children(xmlNodePtr parent, const std::vector<std::shared_ptr<Element>> &children)
{
    return -1;
}

void FileImpl::set_encoding(const std::string &encoding)
{
    m_encoding = encoding;
}

const std::string &FileImpl::get_encoding() const
{
    return m_encoding;
}

} // namespace libxml2

} // namespace xml

} // namespace esysfile
