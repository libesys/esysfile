/*!
 * \file esysfile/xml/signatureverificationresult_xml.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/signatureverificationresult.h"

#include <ostream>

namespace esysfile
{

namespace xml
{

ESYSFILE_API std::ostream &operator<<(std::ostream &os, const SignatureVerificationResult signature_verification_result)
{
    return os;
}

} // namespace xml

} // namespace esysfile
