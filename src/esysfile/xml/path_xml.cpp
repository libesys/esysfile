/*!
 * \file esysfile/xml/path_xml.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/path.h"
#include "esysfile/xml/element.h"

namespace esysfile
{

namespace xml
{

PathItem::PathItem()
{
}

PathItem::PathItem(std::shared_ptr<Element> element)
{
    m_name = element->get_name();
    m_element = element;
}

PathItem::~PathItem()
{
}

void PathItem::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &PathItem::get_name() const
{
    return m_name;
}

void PathItem::set_element(std::shared_ptr<Element> element)
{
    m_element = element;
}

const std::shared_ptr<Element> PathItem::get_element() const
{
    return m_element;
}

std::shared_ptr<Element> PathItem::get_element()
{
    return m_element;
}

Path::Path()
{
}

Path::~Path()
{
}

void Path::push_front(std::shared_ptr<Element> element)
{
    m_items.push_front(element);
}

std::string Path::to_string() const
{
    std::string path;

    for (auto &item : get_items())
    {
        if (!path.empty()) path += ".";
        path += item.get_name();
    }

    return path;
}

const std::deque<PathItem> &Path::get_items() const
{
    return m_items;
}

std::deque<PathItem> &Path::get_items()
{
    return m_items;
}

} // namespace xml

} // namespace esysfile
