/*!
 * \Data esysfile/xml/data_xml.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/data.h"
#include "esysfile/xml/attr.h"

namespace esysfile::xml
{

Data::Data()
    : Element()
{
    set_depth(0);
}

Data::~Data()
{
}

void Data::set_root_node_name(const std::string &name)
{
    set_name(name);

    m_root_node_name = get_name();
    m_root_node_prefix = get_prefix();
}

std::string &Data::get_root_node_name()
{
    return m_root_node_name;
}

const std::string &Data::get_root_node_name() const
{
    return m_root_node_name;
}

void Data::set_root_node_prefix(const std::string &prefix)
{
    m_root_node_prefix = prefix;
    set_prefix(prefix);
}

std::string &Data::get_root_node_prefix()
{
    return m_root_node_prefix;
}

const std::string &Data::get_root_node_prefix() const
{
    return m_root_node_prefix;
}

std::string Data::get_root_node_fullname() const
{
    if (get_root_node_prefix().empty()) return get_root_node_name();

    return get_root_node_prefix() + ":" + get_root_node_name();
}

void Data::set_root_attr_version(const std::string &version)
{
    auto attr_version = get_attr("version");
    if (attr_version == nullptr)
    {
        add_attr("version", version);
        return;
    }
    attr_version->set_value(version);
}

std::string Data::get_root_attr_version() const
{
    auto attr_version = get_attr("version");
    if (attr_version == nullptr) return "";

    return attr_version->get_value();
}

void Data::set_root_attr_name(const std::string &name)
{
    auto attr_name = get_attr("name");
    if (attr_name == nullptr)
    {
        add_attr("name", name);
        return;
    }
    attr_name->set_value(name);
}

std::string Data::get_root_attr_name() const
{
    auto attr_name = get_attr("name");
    if (attr_name == nullptr) return "";

    return attr_name->get_value();
}

void Data::set_dtd(const std::string &dtd)
{
    m_dtd = dtd;
}

const std::string &Data::get_dtd() const
{
    return m_dtd;
}

std::string &Data::get_dtd()
{
    return m_dtd;
}

bool Data::operator==(const Data &other) const
{
    if (get_root_node_name() != other.get_root_node_name()) return false;
    if (get_root_node_prefix() != other.get_root_node_prefix()) return false;
    if (get_root_attr_version() != other.get_root_attr_version()) return false;
    if (get_root_attr_name() != other.get_root_attr_name()) return false;
    if (get_dtd() != other.get_dtd()) return false;

    return Element::operator==(other);
}

bool Data::operator!=(const Data &other) const
{
    return !operator==(other);
}

ESYSFILE_API std::ostream &operator<<(std::ostream &os, const Data &data)
{
    return os;
}

} // namespace esysfile::xml
