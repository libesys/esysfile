/*!
 * \file esysfile/xml/attr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysfile/esysfile_prec.h"
#include "esysfile/xml/attr.h"

#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lexical_cast.hpp>

#include <sstream>

namespace esysfile
{

namespace xml
{

Attr::Attr()
{
}

Attr::~Attr()
{
}

void Attr::set_name(const std::string &name)
{
    m_name = name;
}

std::string &Attr::get_name()
{
    return m_name;
}

const std::string &Attr::get_name() const
{
    return m_name;
}

void Attr::set_value(const char *value)
{
    m_value = value;
}

void Attr::set_value(const std::string &value)
{
    m_value = value;
}

std::string &Attr::get_value()
{
    return m_value;
}

const std::string &Attr::get_value() const
{
    return m_value;
}

int Attr::get_value(unsigned int &value) const
{
    try
    {
        value = boost::lexical_cast<int>(get_value());
    }
    catch (const boost::bad_lexical_cast &)
    {
        return -1;
    }
    return 0;
}

int Attr::get_value(int &value) const
{
    try
    {
        value = boost::lexical_cast<int>(get_value());
    }
    catch (const boost::bad_lexical_cast &)
    {
        return -1;
    }
    return 0;
}

void Attr::set_value(int value)
{
    std::ostringstream oss;

    oss << value;

    set_value(oss.str());
}

void Attr::set_value(bool value)
{
    if (value)
        m_value = "true";
    else
        m_value = "false";
}

int Attr::get_value(bool &value)
{
    std::string value_str = get_value();

    boost::algorithm::to_lower(value_str);
    if (value_str == "true")
        value = true;
    else if (value_str == "false")
        value = false;
    else
        return -1;
    return 0;
}

int Attr::get_value_yes_no(bool &value)
{
    std::string value_str = get_value();

    boost::algorithm::to_lower(value_str);
    if (value_str == "yes")
        value = true;
    else if (value_str == "no")
        value = false;
    else
        return -1;
    return 0;
}

void Attr::set_value(double value)
{
    std::ostringstream oss;

    oss << value;

    set_value(oss.str());
}

int Attr::get_value(double &value)
{
    try
    {
        value = boost::lexical_cast<double>(get_value());
    }
    catch (const boost::bad_lexical_cast &)
    {
        return -1;
    }
    return 0;
}

bool Attr::operator==(const Attr &other) const
{
    if (get_name() != other.get_name()) return false;
    if (get_value() != other.get_value()) return false;

    return true;
}

bool Attr::operator!=(const Attr &other) const
{
    return !operator==(other);
}

} // namespace xml

} // namespace esysfile
