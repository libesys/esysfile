/*!
 * \file esysfile_t/xml/fixture/writereadtest.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile_t/xml/fixture/database.h"

#include <esysfile/xml/filebase.h>
#include <esysfile/xml/data.h>

#include <memory>
#include <string>

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

class WriteReadTest
{
public:
    WriteReadTest(const std::string &filename = "", esysfile::xml::FileBase *xml_write = nullptr,
                  esysfile::xml::FileBase *xml_read = nullptr, xml::fixture::DataBase *data = nullptr);
    virtual ~WriteReadTest();

    void set_filename(const std::string &filename);
    const std::string &get_filename() const;

    void set_xml_write(esysfile::xml::FileBase *xml_write);
    esysfile::xml::FileBase *get_xml_write() const;

    void set_xml_read(esysfile::xml::FileBase *xml_read);
    esysfile::xml::FileBase *get_xml_read() const;

    void set_data(xml::fixture::DataBase *data);
    xml::fixture::DataBase *get_data() const;

    void run();

private:
    std::string m_filename;
    esysfile::xml::FileBase *m_xml_write = nullptr;
    esysfile::xml::FileBase *m_xml_read = nullptr;
    esysfile_t::xml::fixture::DataBase *m_data = nullptr;
};

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
