/*!
 * \file esysfile_t/xml/fixture/data02.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile_t/xml/fixture/database.h"

#include <esysfile/xml/element.h>

#include <chrono>

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

class Data02 : public DataBase
{
public:
    Data02();
    ~Data02();

    void create(std::shared_ptr<esysfile::xml::Data> data) override;
    void test(std::shared_ptr<esysfile::xml::Data> data) override;

    void add_device(std::shared_ptr<esysfile::xml::Element> devices_el);
    void add_settings(std::shared_ptr<esysfile::xml::Element> device_el);
    void add_measurements(std::shared_ptr<esysfile::xml::Element> device_el);
    void add_measurement(std::shared_ptr<esysfile::xml::Element> measurements_el);

    void set_measurements_count(std::size_t measurements_count);
    std::size_t get_measurements_count() const;

    std::chrono::system_clock::time_point &get_meas_time();
    const std::chrono::system_clock::time_point &get_meas_time() const;

private:
    std::size_t m_measurements_count = 100;
    std::chrono::system_clock::time_point m_meas_time = std::chrono::system_clock::now();
};

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
