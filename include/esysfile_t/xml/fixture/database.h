/*!
 * \file esysfile_t/xml/fixture/database.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esysfile/xml/data.h>

#include <memory>

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

class DataBase
{
public:
    DataBase(const std::string &root_name);
    virtual ~DataBase();

    void set_root_name(const std::string &root_name);
    const std::string &get_root_name() const;

    virtual void create(std::shared_ptr<esysfile::xml::Data> data) = 0;
    virtual void test(std::shared_ptr<esysfile::xml::Data> data) = 0;

private:
    std::string m_root_name;
};

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
