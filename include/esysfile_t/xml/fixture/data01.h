/*!
 * \file esysfile_t/xml/fixture/data01.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile_t/xml/fixture/database.h"

namespace esysfile_t
{

namespace xml
{

namespace fixture
{

class Data01 : public DataBase
{
public:
    Data01();
    ~Data01();

    void create(std::shared_ptr<esysfile::xml::Data> data) override;
    void test(std::shared_ptr<esysfile::xml::Data> data) override;
};

} // namespace fixture

} // namespace xml

} // namespace esysfile_t
