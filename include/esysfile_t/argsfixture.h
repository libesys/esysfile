/*!
 * \file esysfile_t/argsfixture.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <boost/filesystem.hpp>

#ifdef _MSC_VER
#pragma warning(default : 4996)
#endif

struct ArgsFixture
{
    ArgsFixture();

    int m_argc;
    char **m_argv;
    std::string m_test_file_path;
    boost::filesystem::path m_abs_test_file_path;
    boost::filesystem::path m_abs_temp_path;
};
