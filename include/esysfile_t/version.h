/*!
 * \file esysfile/version.h
 * \brief Version info for esysfile_t
 *
 * \cond
 *__legal_b__
 *
 *Copyright (c) 2014-2023 Michel Gillet
 *Distributed under the MIT License.
 *(See accompanying file LICENSE.txt or
 *copy at https://opensource.org/licenses/MIT)
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYSFILE_T_VERSION_H__
#define __ESYSFILE_T_VERSION_H__

// Bump-up with each new version
#define ESYSFILE_T_MAJOR_VERSION 0
#define ESYSFILE_T_MINOR_VERSION 0
#define ESYSFILE_T_RELEASE_NUMBER 1
#define ESYSFILE_T_VERSION_STRING _T("esysfile_t 0.0.1")

// Must be updated manually as well each time the version above changes
#define ESYSFILE_T_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSFILE_T_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSFILE_T_VERSION_NUMBER \
    (ESYSFILE_T_MAJOR_VERSION * 1000) + (ESYSFILE_T_MINOR_VERSION * 100) + ESYSFILE_T_RELEASE_NUMBER
#define ESYSFILE_T_BETA_NUMBER 1
#define ESYSFILE_T_VERSION_FLOAT                                                                       \
    ESYSFILE_T_MAJOR_VERSION + (ESYSFILE_T_MINOR_VERSION / 10.0) + (ESYSFILE_T_RELEASE_NUMBER / 100.0) \
        + (ESYSFILE_T_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSFILE_T_CHECK_VERSION(major, minor, release)                                                                \
    (ESYSFILE_T_MAJOR_VERSION > (major) || (ESYSFILE_T_MAJOR_VERSION == (major) && ESYSFILE_T_MINOR_VERSION > (minor)) \
     || (ESYSFILE_T_MAJOR_VERSION == (major) && ESYSFILE_T_MINOR_VERSION == (minor)                                    \
         && ESYSFILE_T_RELEASE_NUMBER >= (release)))

#endif
