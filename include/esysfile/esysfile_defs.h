/*!
 * \file esysfile/esysfile_defs.h
 * \brief Definitions needed for esysfile
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSFILE_EXPORTS
#define ESYSFILE_API __declspec(dllexport)
#elif ESYSFILE_USE
#define ESYSFILE_API __declspec(dllimport)
#else
#define ESYSFILE_API
#endif

#include "esysfile/autolink.h"
