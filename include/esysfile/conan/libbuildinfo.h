/*!
 * \file esysfile/conan/libbuildinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#include <string>
#include <vector>

namespace esysfile::conan
{

/*! \class LibBuildInfo esysfile/conan/libbuildinfo.h "esysfile/conan/libbuildinfo.h"
 * \brief
 */
class ESYSFILE_API LibBuildInfo
{
public:
    //! Constructor
    LibBuildInfo();

    //! Destructor
    ~LibBuildInfo();

    void set_name(const std::string &name);
    const std::string &get_name() const;

    std::vector<std::string> &get_include_dirs();
    const std::vector<std::string> &get_include_dirs() const;

    std::vector<std::string> &get_lib_dirs();
    const std::vector<std::string> &get_lib_dirs() const;

    std::vector<std::string> &get_bin_dirs();
    const std::vector<std::string> &get_bin_dirs() const;

    std::vector<std::string> &get_res_dirs();
    const std::vector<std::string> &get_res_dirs() const;

    std::vector<std::string> &get_build_dirs();
    const std::vector<std::string> &get_build_dirs() const;

    std::vector<std::string> &get_libs();
    const std::vector<std::string> &get_libs() const;

    std::vector<std::string> &get_system_libs();
    const std::vector<std::string> &get_system_libs() const;

    //! Equal to comparison operator
    bool operator==(const LibBuildInfo &other) const;

    //! Not equal to comparison operator
    bool operator!=(const LibBuildInfo &other) const;

protected:
    //!< \cond DOXY_IMPL
    bool equal(const LibBuildInfo &other) const;

private:
    std::string m_name;
    std::vector<std::string> m_include_dirs;
    std::vector<std::string> m_lib_dirs;
    std::vector<std::string> m_bin_dirs;
    std::vector<std::string> m_res_dirs;
    std::vector<std::string> m_build_dirs;
    std::vector<std::string> m_libs;
    std::vector<std::string> m_system_libs;
    //!< \endcond
};

} // namespace esysfile::conan
