/*!
 * \file esysfile/conan/buildinfotxt.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/conan/libbuildinfo.h"

#include "esysfile/ini/file.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

class ESYSFILE_API esysfile::ini::File;

namespace esysfile::conan
{

/*! \class BuildInfoTxt esysfile/conan/buildinfotxt.h "esysfile/conan/buildinfotxt.h"
 * \brief
 */
class ESYSFILE_API BuildInfoTxt
{
public:
    //! Constructor
    BuildInfoTxt();

    //! Constructor
    BuildInfoTxt(const std::string &file_path);

    //! Destructor
    ~BuildInfoTxt();

    void set_file_path(const std::string &file_path);
    const std::string &get_file_path() const;

    std::shared_ptr<LibBuildInfo> get_lib_build_info();
    std::vector<std::shared_ptr<LibBuildInfo>> &get_lib_build_infos();
    const std::vector<std::shared_ptr<LibBuildInfo>> &get_lib_build_infos() const;
    std::map<std::string, std::shared_ptr<LibBuildInfo>> &get_lib_build_info_map();
    const std::map<std::string, std::shared_ptr<LibBuildInfo>> &get_lib_build_info_map() const;

    std::shared_ptr<LibBuildInfo> find(const std::string &name);

    int read(const std::string &file_path = "");

    //! Equal to comparison operator
    bool operator==(const BuildInfoTxt &other) const;

    //! Not equal to comparison operator
    bool operator!=(const BuildInfoTxt &other) const;

protected:
    //!< \cond DOXY_IMPL
    bool equal(const BuildInfoTxt &other) const;

private:
    int process_section(std::shared_ptr<ini::Section> section);
    int process_section(const std::string &section_kind, std::shared_ptr<ini::Section> section,
                        std::shared_ptr<LibBuildInfo> lib_info);
    std::shared_ptr<LibBuildInfo> find_or_new(const std::string &name);

    std::string m_file_path;
    std::unique_ptr<ini::File> m_ini_file;
    std::shared_ptr<LibBuildInfo> m_lib_build_info;
    std::vector<std::shared_ptr<LibBuildInfo>> m_lib_build_infos;
    std::map<std::string, std::shared_ptr<LibBuildInfo>> m_lib_build_info_map;
    //!< \endcond
};

} // namespace esysfile::conan
