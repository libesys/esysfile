/*!
 * \file esysfile/xml/filebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/data.h"
#include "esysfile/xml/signatureverificationresult.h"
#include "esysfile/xml/functorgen.h"

#include <string>
#include <fstream>
#include <map>
#include <memory>

namespace esysfile::xml
{

/*! \class FileBase esysfile/xml/filebase.h "esysfile/xml/filebase.h"
 * \brief Base class of all XML file implementation
 */
class ESYSFILE_API FileBase
{
public:
    //! Constructor
    FileBase(const std::string &filename = "");

    //! Destructor
    virtual ~FileBase();

    /*! \brief Write the XML data to a file
     * \param[in] filename the filename
     * \return 0 if successful, < 0 otherwise
     */
    virtual int write(const std::string &filename = "") = 0;

    /*! \brief Write the XML data to an output stream
     * \param[in] output_stream the stream
     * \return 0 if successful, < 0 otherwise
     */
    virtual int write(std::ostream &ouput_stream) = 0;

    /*! \brief Read the XML data from a file
     * \param[in] filename the filename
     * \return 0 if successful, < 0 otherwise
     */
    virtual int read(const std::string &filename = "") = 0;

    /*! \brief Read the XML data from an input stream
     * \param[in] filename the filename
     * \return 0 if successful, < 0 otherwise
     */
    virtual int read(std::istream &input_stream) = 0;

    /*! \brief Set the filename
     * \param[in] filename the filename
     */
    void set_file_name(const std::string &filename);

    /*! \brief Get the filename
     * \return the filename
     */
    std::string &get_file_name();

    /*! \brief Get the filename
     * \return the filename
     */
    const std::string &get_file_name() const;

    /*! \brief Set the XML data
     * \param[in] data the XML data
     */
    void set_data(std::shared_ptr<Data> data);

    /*! \brief Get the XML data
     * \return the XML data
     */
    const std::shared_ptr<Data> get_data() const;

    /*! \brief Get the XML data
     * \return the XML data
     */
    std::shared_ptr<Data> get_data();

    /*! \brief Set the name of the root node
     * \param[in] name the name of the root node
     */
    void set_root_node_name(const std::string &name);

    /*! \brief Get the name of the root node
     * \return the name of the root node
     */
    std::string get_root_node_name() const;

    /*! \brief Set the prefix of the root node
     * \param[in] prefix the prefix of the root node
     */
    void set_root_node_prefix(const std::string &prefix);

    /*! \brief Get the prefix of the root node
     * \return the prefix of the root node
     */
    std::string &get_root_node_prefix();

    /*! \brief Get the prefix of the root node
     * \return the prefix of the root node
     */
    const std::string &get_root_node_prefix() const;

    /*! \brief Get the full name of the root node
     * \return the full name of the root node
     */
    std::string get_root_node_fullname() const;

    /*! \brief Set the 'version' root node attribute
     * \param[in] version the version
     */
    void set_root_attr_version(const std::string &version);

    /*! \brief Get the 'version' root node attribute
     * \return the version
     */
    std::string get_root_attr_version() const;

    /*! \brief Set the 'name' root node attribute
     * \param[in] name the name
     */
    void set_root_attr_name(const std::string &name);

    /*! \brief Get the 'name' root node attribute
     * \return the name
     */
    std::string get_root_attr_name() const;

    /*! \brief Set the indent string to use
     * \param[in] indent the indent string
     */
    void set_indent(const std::string &indent);

    /*! \brief Get the indent string to use
     * \return the indent string
     */
    const std::string &get_indent() const;

    /*! \brief Get the indent string to use
     * \return the indent string
     */
    std::string &get_indent();

    /*! \brief Tells if the XML file can be digitally signed or not
     * \return true if digital signature are supported; false otherwise
     */
    virtual bool get_signature_support() const;

    /*! \brief Set the PEM key file to use to sign the XML file
     * \param[in] key_file the PEM key file
     */
    void set_key_file(const std::string &key_file);

    /*! \brief Get the PEM key file to use to sign the XML file
     * \return the PEM key file
     */
    const std::string &get_key_file() const;

    /*! \brief Set the PEM certification file to use to sign the XML file
     * \param[in] cert_file the PEM certification file
     */
    void set_cert_file(const std::string &cert_file);

    /*! \brief Get the PEM certification file to use to sign the XML file
     * \return the PEM certification file
     */
    const std::string &get_cert_file() const;

    /*! \brief Set the if the XML file should be digitally signed
     * \param[in] digitally_signed true if the file should be digitallly signed
     */
    void set_digitally_signed(bool digitally_signed);

    /*! \brief Get the if the XML file should be or is digitally signed
     * \return true if the file should be digitallly signed; false if not
     */
    bool get_digitally_signed() const;

    /*! \brief Add a trusted PEM certificate file
     * \param[in] trusted_cert_file the trusted PEM certificate file to add
     */
    void add_trusted_cert_file(const std::string &trusted_cert_file);

    /*! \brief Get the list of trusted PEM certificate files
     * \return the list of trusted PEM certificate files
     */
    const std::vector<std::string> &get_trusted_cert_files() const;

    /*! \brief Get the result of the signature verification when a file was read
     * \return the result of the signature verification
     */
    SignatureVerificationResult get_signature_verification_result() const;

    void set_functor(const std::string &xpath, FunctorGen *gen);
    FunctorGen *get_functor(const std::string &xpath);

protected:
    //!< \cond DOXY_IMPL

    /*! \brief Get or create a XML data if not existing
     * \return the XML data
     */
    std::shared_ptr<Data> get_data_or_new();

protected:
    /*! \brief Set the result of the signature verification when a file was read
     * \param[in] signature_verification_result the result of the signature verification
     */
    void set_signature_verification_result(SignatureVerificationResult signature_verification_result);

private:
    std::string m_filename;          //!< The file name
    std::shared_ptr<Data> m_data;    //!< The XML data
    std::string m_empty;             //!< An empty string
    std::ofstream *m_ofs = nullptr;  //!< Output stream
    std::string m_indent = "  ";     //!< The indent string
    std::string m_key_file;          //!< The PEM key file to use to sign the XML file
    std::string m_cert_file;         //!< The PEM certificate file to use to sign the XML file
    bool m_digitally_signed = false; //!< If the file is or should be digitally signed

    //! The result of the signature verification after a file was read
    SignatureVerificationResult m_signature_verification_result = SignatureVerificationResult::NO_SIGNATURE;
    std::vector<std::string> m_trusted_cert_files; //!< List of trusted PEM certificate files
    std::map<std::string, FunctorGen *> m_map_funcgens;
    //!< \endcond
};

} // namespace esysfile::xml
