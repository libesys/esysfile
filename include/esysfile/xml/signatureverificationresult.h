/*!
 * \file esysfile/xml/signatureverificationresult.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#include <ostream>

namespace esysfile
{

namespace xml
{

class ESYSFILE_API Attr;
class ESYSFILE_API Data;

/*! \class SignatureVerificationResult esysfile/xml/signatureverificationresult.h
 * "esysfile/xml/signatureverificationresult.h" \brief
 */
enum class SignatureVerificationResult
{
    NO_SIGNATURE,
    VALID_SIGNATURE,
    SIGNATURE_DATA_NO_MATCH,
    VERIFICATION_FAILED
};

ESYSFILE_API std::ostream &operator<<(std::ostream &os,
                                      const SignatureVerificationResult signature_verification_result);

} // namespace xml

} // namespace esysfile
