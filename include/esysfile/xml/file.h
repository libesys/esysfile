/*!
 * \file esysfile/xml/element.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#if !defined(ESYSFILE_XML_USE_POCO) && !defined(ESYSFILE_XML_USE_LIBXML2)
#define ESYSFILE_XML_USE_POCO
#endif

#ifdef ESYSFILE_XML_USE_POCO
#include "esysfile/xml/poco/file.h"
#elif defined(ESYSFILE_XML_USE_LIBXML2)
#include "esysfile/xml/libxml2/file.h"
#endif
