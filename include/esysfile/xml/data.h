/*!
 * \file esysfile/xml/data.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/element.h"

#include <string>
#include <ostream>

namespace esysfile
{

namespace xml
{

/*! \class Data esysfile/xml/data.h "esysfile/xml/data.h"
 * \brief The data model of a XML file
 */
class ESYSFILE_API Data : public Element
{
public:
    //! Constructor
    Data();

    // Destructor
    virtual ~Data();

    /*! \brief Set the name of the root node
     * \param[in] name the name of the root node
     */
    void set_root_node_name(const std::string &name);

    /*! \brief Get the name of the root node
     * \return the name of the root node
     */
    std::string &get_root_node_name();

    /*! \brief Get the name of the root node
     * \return the name of the root node
     */
    const std::string &get_root_node_name() const;

    /*! \brief Set the prefix of the root node
     * \param[in] prefix the prefix of the root node
     */
    void set_root_node_prefix(const std::string &prefix);

    /*! \brief Get the prefix of the root node
     * \return the prefix of the root node
     */
    std::string &get_root_node_prefix();

    /*! \brief Get the prefix of the root node
     * \return the prefix of the root node
     */
    const std::string &get_root_node_prefix() const;

    /*! \brief Get the full name of the root node
     * \return the full name of the root node
     */
    std::string get_root_node_fullname() const;

    /*! \brief Set the 'version' root node attribute
     * \param[in] version the version
     */
    void set_root_attr_version(const std::string &version);

    /*! \brief Get the 'version' root node attribute
     * \return the version
     */
    std::string get_root_attr_version() const;

    /*! \brief Set the 'name' root node attribute
     * \param[in] name the name
     */
    void set_root_attr_name(const std::string &name);

    /*! \brief Get the 'name' root node attribute
     * \return the name
     */
    std::string get_root_attr_name() const;

    /*! \brief Set the DTD string
     * \param[in] dtd the DTD string
     */
    void set_dtd(const std::string &dtd);

    /*! \brief Get the DTD string
     * \return the DTD string
     */
    const std::string &get_dtd() const;

    /*! \brief Get the DTD string
     * \return the DTD string
     */
    std::string &get_dtd();

    //! Equal to comparison operator
    bool operator==(const Data &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Data &other) const;

private:
    //!< \cond DOXY_IMPL
    std::string m_root_node_name;   //!< The root element name
    std::string m_root_node_prefix; //!< The root element prefix
    std::string m_dtd;              //!< The DTD string
    //!< \endcond
};

ESYSFILE_API std::ostream &operator<<(std::ostream &os, const Data &data);

} // namespace xml

} // namespace esysfile
