/*!
 * \file esysfile/xml/libxml2/file.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/filebase.h"
#include "esysfile/xml/functorgen.h"

#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <memory>

namespace esysfile::xml::libxml2
{

class ESYSFILE_API FileImpl;

/*! \class File esysfile/xml/libxml2/file.h "esysfile/xml/libxml2/file.h"
 * \brief A XML file implemeted using libxml2
 */
class ESYSFILE_API File : public FileBase
{
public:
    friend class ESYSFILE_API FileImpl;

    //! Constructor
    /*!
     * \param[in] filename the filename
     */
    File(const std::string &filename = "");

    //! Destructor
    ~File() override;

    int write(const std::string &filename = "") override;
    int write(std::ostream &output_stream) override;
    int read(const std::string &filename = "") override;
    int read(std::istream &input_stream) override;

    bool get_signature_support() const override;

    /*! \brief Get the PIML object
     * \return the PIML object
     */
    FileImpl *get_impl();

private:
    //!< \cond DOXY_IMPL
    std::unique_ptr<FileImpl> m_impl;
    //!< \endcond
};

} // namespace esysfile::xml::libxml2

namespace esysfile::xml
{

#ifdef ESYSFILE_XML_USE_LIBXML2
using namespace libxml2;
#endif

} // namespace esysfile::xml
