/*!
 * \file esysfile/xml/path.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#include <string>
#include <deque>
#include <memory>

namespace esysfile
{

namespace xml
{

class ESYSFILE_API Element;

class ESYSFILE_API PathItem
{
public:
    PathItem();
    PathItem(std::shared_ptr<Element> element);
    virtual ~PathItem();

    void set_name(const std::string &name);
    const std::string &get_name() const;

    void set_element(std::shared_ptr<Element> element);
    const std::shared_ptr<Element> get_element() const;
    std::shared_ptr<Element> get_element();

protected:
    std::string m_name;
    std::shared_ptr<Element> m_element;
};

class ESYSFILE_API Path
{
public:
    Path();
    virtual ~Path();

    void push_front(std::shared_ptr<Element> element);

    std::string to_string() const;

    const std::deque<PathItem> &get_items() const;
    std::deque<PathItem> &get_items();

protected:
    std::deque<PathItem> m_items;
};

} // namespace xml

} // namespace esysfile
