/*!
 * \file esysfile/xml/functorgen.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

namespace esysfile
{

namespace xml
{

class ESYSFILE_API Element;

class ESYSFILE_API FunctorGen
{
public:
    FunctorGen();
    virtual ~FunctorGen();

    virtual Element *New() = 0;

protected:
};

template<typename T>
class FunctorGen_t : public FunctorGen
{
public:
    FunctorGen_t()
    {
    }

    virtual ~FunctorGen_t()
    {
    }

    virtual Element *New()
    {
        return new T();
    }
};

} // namespace xml

} // namespace esysfile
