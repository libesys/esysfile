/*!
 * \file esysfile/xml/attr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#include <string>

namespace esysfile
{

namespace xml
{

/*! \class Attr esysfile/xml/attr.h "esysfile/xml/attr.h"
 * \brief A XML attribute
 */
class ESYSFILE_API Attr
{
public:
    //! Constructor
    Attr();

    //! Destructor
    virtual ~Attr();

    /*! \brief Set the name of the attribute
     * \param[in] name the name of the attribute
     */
    void set_name(const std::string &name);

    /*! \brief Get the name of the attribute
     * \return the name of the attribute
     */
    std::string &get_name();

    /*! \brief Get the name of the attribute
     * \return the name of the attribute
     */
    const std::string &get_name() const;

    /*! \brief Set the value of the attribute
     * \param[in] value the value of the attribute
     */
    void set_value(const char *value);

    /*! \brief Set the value of the attribute
     * \param[in] value the value of the attribute
     */
    void set_value(const std::string &value);

    /*! \brief Get the value of the attribute
     * \return the value of the attribute
     */
    std::string &get_value();

    /*! \brief Get the value of the attribute
     * \return the value of the attribute
     */
    const std::string &get_value() const;

    /*! \brief Set the value of the attribute
     * \param[in] value the value of the attribute
     */
    void set_value(int value);

    /*! \brief Get the value of the attribute as an unsigned integer
     * \param[out] value the value of the attribute
     */
    int get_value(unsigned int &value) const;

    /*! \brief Get the value of the attribute as an integer
     * \param[out] value the value of the attribute
     */
    int get_value(int &value) const;

    /*! \brief Set the value of the attribute
     * \param[in] value the value of the attribute
     */
    void set_value(bool value);

    /*! \brief Get the value of the attribute
     * \param[out] value the value of the attribute
     * \return 0 if the value was a boolean, < 0 otherwise
     */
    int get_value(bool &value);

    /*! \brief Get the value of the attribute
     * \param[out] value the value of the attribute
     * \return 0 if the value was a boolean, < 0 otherwise
     */
    int get_value_yes_no(bool &value);

    /*! \brief Set the value of the attribute
     * \param[in] value the value of the attribute
     */
    void set_value(double value);

    /*! \brief Get the value of the attribute
     * \param[out] value the value of the attribute
     * \return 0 if the value was a boolean, < 0 otherwise
     */
    int get_value(double &value);

    //! Equal to comparison operator
    bool operator==(const Attr &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Attr &other) const;

protected:
    //!< \cond DOXY_IMPL
    std::string m_name;  //!< The name
    std::string m_value; //!< The value
    //!< \endcond
};

} // namespace xml

} // namespace esysfile
