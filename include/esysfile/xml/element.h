/*!
 * \file esysfile/xml/element.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/path.h"

#include <string>
#include <vector>
#include <map>
#include <memory>

namespace esysfile
{

namespace xml
{

class ESYSFILE_API Attr;
class ESYSFILE_API Data;

/*! \class Element esysfile/xml/element.h "esysfile/xml/element.h"
 * \brief One element of an XML file
 */
class ESYSFILE_API Element : public std::enable_shared_from_this<Element>
{
public:
    //! Constructor
    Element(Data *data = nullptr);

    //! Destructor
    virtual ~Element();

    virtual void clear();

    /*! \brief Set the name of the XML element
     * \param[in] name the name
     */
    void set_name(const std::string &name);

    /*! \brief Get the name of the XML element
     * \return the name
     */
    std::string &get_name();

    /*! \brief Get the name of the XML element
     * \return the name
     */
    const std::string &get_name() const;

    /*! \brief Set the prefix of the XML element
     * \param[in] prefix the prefix
     */
    void set_prefix(const std::string &prefix);

    /*! \brief Get the prefix of the XML element
     * \return the prefix
     */
    std::string &get_prefix();

    /*! \brief Get the prefix of the XML element
     * \return the prefix
     */
    const std::string &get_prefix() const;

    /*! \brief Get the fullname of the XML element
     * \return the full name [<prefix>:]<name>
     */
    std::string get_fullname() const;

    void set_data(Data *data);
    Data *get_data();

    /*! \brief Add and return a new XML element
     * \param[in] name the name of the new XML element
     * \return the new XML element
     */
    std::shared_ptr<Element> add_element(const std::string &name);

    /*! \brief Add an XML element
     * \param[in] el the XML element to add
     */
    void add_element(std::shared_ptr<Element> el);

    /*! \brief Add new XML element to a given parent element
     * \param[in] parent the parent XML element
     * \param[in] name the name of the new XML element
     * \param[in] value the value of the new XML element
     */
    void add_element(std::shared_ptr<Element> parent, const std::string &name, const std::string &value);

    /*! \brief Add new XML element to a given parent element, its value a list of strings
     * \param[in] parent the parent XML element
     * \param[in] name the name of the new XML element
     * \param[in] values the list of strings to set the value of new XML element
     * \param[in] comma_separated if true, the strings will be comma separated, othersize separated by 1 space
     */
    void add_element(std::shared_ptr<Element> parent, const std::string &xml_name,
                     const std::vector<std::string> &values, bool comma_separated = true);

    /*! \brief Get the number of child elements
     * \return the number of child elements
     */
    std::size_t get_element_count();

    /*! \brief Get the number of child elements with a given name
     * \return the number of child elements
     */
    std::size_t get_element_count(const std::string &name);

    /*! \brief Get a given element by its index
     * \param[in] idx the index of the child element in the list
     * \return the child element
     */
    std::shared_ptr<Element> get_element(std::size_t idx = 0);

    /*! \brief Get a nth element with a given name
     * \param[in] name the name of the element
     * \param[in] idx the index of the child element with a given name
     * \return the child element
     */
    std::shared_ptr<Element> get_element(const std::string &name, std::size_t idx = 0);

    /*! \brief Get all child elements
     * \return all child elements
     */
    std::vector<std::shared_ptr<Element>> &get_elements();

    /*! \brief Get all child elements
     * \return all child elements
     */
    const std::vector<std::shared_ptr<Element>> &get_elements() const;

    /*! \brief Get an element with a given path
     * \return the child element
     */
    std::shared_ptr<Element> get_element_by_path(const std::string &path);

    /*! \brief Add an attribute
     * \param[in] attr the attribute
     */
    void add_attr(std::shared_ptr<Attr> attr);

    /*! \brief Add an attribute
     * \param[in] name the name of the attribute
     * \param[in] value the value of the attribute
     */
    void add_attr(const std::string &name, const char *value);

    /*! \brief Add an attribute
     * \param[in] name the name of the attribute
     * \param[in] value the value of the attribute
     */
    void add_attr(const std::string &name, const std::string &value);

    /*! \brief Add an attribute
     * \param[in] name the name of the attribute
     * \param[in] value the value of the attribute
     */
    void add_attr(const std::string &name, int value);

    /*! \brief Add an attribute
     * \param[in] name the name of the attribute
     * \param[in] value the value of the attribute
     */
    void add_attr(const std::string &name, double value);

    /*! \brief Add an attribute
     * \param[in] name the name of the attribute
     * \param[in] value the value of the attribute
     */
    void add_attr(const std::string &name, bool value);

    /*! \brief Get the number of attributes
     * \return the number of attributes
     */
    std::size_t get_attr_count();

    /*! \brief Get an attribute by its index
     * \return the attribute
     */
    std::shared_ptr<Attr> get_attr(std::size_t idx = 0);

    /*! \brief Get an attribute by its name
     * \return the attribute
     */
    std::shared_ptr<Attr> get_attr(const std::string &name) const;

    /*! \brief Get all attributes
     * \return all attributes
     */
    std::vector<std::shared_ptr<Attr>> &get_attrs();

    /*! \brief Get all attributes
     * \return all attributes
     */
    const std::vector<std::shared_ptr<Attr>> &get_attrs() const;

    /*! \brief Tells if the element has a value
     * \return true if the element has a value, false otherwise
     */
    bool has_value();

    int get_path(Path &path);
    int get_path(std::shared_ptr<Path> path);

    void set_parent(std::weak_ptr<Element> parent);
    std::weak_ptr<Element> get_parent() const;
    std::weak_ptr<Element> get_parent();

    /*! \brief Get the value of the element
     * \return the value of the element
     */
    std::string &get_value();

    /*! \brief Get the value of the element
     * \return the value of the element
     */
    const std::string &get_value() const;

    /*! \brief Set the value of the element
     * \param[in] value the value of the element
     */
    void set_value(const std::string &value);

    /*! \brief Get the value of the element as an unsigned integer
     * \param[out] value the value
     * \return true if the conversion was succesful, false otherwise
     */
    int get_value(unsigned int &value);

    /*! \brief Get the value of the element as an integer
     * \param[out] value the value
     * \return true if the conversion was succesful, false otherwise
     */
    int get_value(int &value);

    /*! \brief Set line number of the element
     * \param[in] line_number the line number
     */
    void set_line_number(int line_number);

    /*! \brief Get line number of the element
     * \return the line number
     */
    int get_line_number() const;

    //! Equal to comparison operator
    bool operator==(const Element &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Element &other) const;

    int get_depth() const;

    static int get_count();
    static int get_max_count();

protected:
    //!< \cond DOXY_IMPL
    void set_depth(int depth);
    void inc_depth();

private:
    static int s_count;
    static int s_max_count;

    std::string m_name;                               //!< The name of the element
    std::string m_prefix;                             //!< The prefix of the element
    std::string m_value;                              //!< The value
    int m_line_number = -1;                           //!< The line number
    Data *m_data = nullptr;                           //!<
    std::vector<std::shared_ptr<Attr>> m_attrs;       //!< The attributes
    std::vector<std::shared_ptr<Element>> m_elements; //!< The child elements
    std::weak_ptr<Element> m_parent;
    std::map<std::string, std::shared_ptr<Element>> m_elements_map; //!< Map of the child elements by name
    std::map<std::string, std::vector<std::shared_ptr<Element>>>
        m_list_elements_map; //!< Map of lists of child elements by name
    int m_id = 0;
    int m_depth = -1;
    //!< \endcond
};

} // namespace xml

} // namespace esysfile
