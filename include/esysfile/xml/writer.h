/*!
 * \file esysfile/xml/writer.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/data.h"

#include <string>
#include <fstream>
#include <memory>

namespace esysfile
{

namespace xml
{

/*! \class Writer esysfile/xml/writer.h "esysfile/xml/writer.h"
 * \brief Simple XML file writer
 */
class ESYSFILE_API Writer
{
public:
    //! Constructor
    /*!
     * \param[in] filename the filename to be written
     */
    Writer(const std::string &filename = "");

    //! Destructor
    virtual ~Writer();

    /*! \brief Set the filename to write
     * \param[in] filename the filename
     */
    void set_filename(const std::string &filename);

    /*! \brief Get the filename to write
     * \return the filename
     */
    const std::string &get_filename() const;

    /*! \brief Set the XML data to write
     * \param[in] data the XML data to write
     */
    void set_data(std::shared_ptr<Data> data);

    /*! \brief Get the XML data to write
     * \return the XML data to write
     */
    std::shared_ptr<Data> get_data();

    /*! \brief Get the XML data to write
     * \return the XML data to write
     */
    const std::shared_ptr<Data> get_data() const;

    /*! \brief Write the XML data to a file
     * \param[in] filename the filename to write
     * \param[in] data the XML data to write to the file
     * \return 0 if successfull, < 0 otherwise
     */
    int write(const std::string &filename = "", std::shared_ptr<Data> data = nullptr);

    /*! \brief Write the XML data to an output stream
     * \param[in] os the output stream
     * \param[in] data the XML data to write to the file
     * \return 0 if successfull, < 0 otherwise
     */
    int write(std::ostream &os, std::shared_ptr<Data> data = nullptr);

    /*! \brief Write the XML header to an output stream
     * \param[in] os the output stream
     */
    void write_xml_header(std::ostream &os);

    /*! \brief Write the DTD to an output stream
     * \param[in] os the output stream
     */
    void write_dtd(std::ostream &os);

    /*! \brief Write an XML element to an output stream
     * \param[in] os the output stream
     * \param[in] el the XML element
     */
    void write_element(std::ostream &os, std::shared_ptr<Element> el);

    /*! \brief Write the attributes of an XML element to an output stream
     * \param[in] os the output stream
     * \param[in] el the XML element
     */
    void write_attributes(std::ostream &os, std::shared_ptr<Element> el);

    /*! \brief Write one XML attribute to an output stream
     * \param[in] os the output stream
     * \param[in] attr the XML attribute
     */
    void write_attribute(std::ostream &os, std::shared_ptr<Attr> attr);

    /*! \brief Add a full element
     * \param[in] full_element a full element
     */
    void add_full_element(const std::string &full_element);

    /*! \brief Tell if it's a full element
     * \return true if full_element is a full element; false otherwise
     */
    bool is_full_element(const std::string &full_element) const;

    /*! \brief Encode a string to be written in the XML file
     * \param[in] text the input string
     * \return the encoded string
     */
    std::string encode(const std::string &text);

    /*! \brief Set if single quote are escaped
     * \param[in] escape_single_quote true if single quote are escaped; false if not
     */
    void set_escape_single_quote(bool escape_single_quote);

    /*! \brief Get if single quote are escaped
     * \return true if single quote are escaped; false if not
     */
    bool get_escape_single_quote() const;

    /*! \brief Set if double quote are escaped
     * \param[in] escape_double_quote true if double quote are escaped; false if not
     */
    void set_escape_double_quote(bool escape_double_quote);

    /*! \brief Get if double quote are escaped
     * \return true if double quote are escaped; false if not
     */
    bool get_escape_double_quote() const;

    /*! \brief Set the number of characters of the indent
     * \param[in] indent the number of characters of the indent
     */
    void set_indent(std::size_t indent);

    /*! \brief Get the number of characters of the indent
     * \return the number of characters of the indent
     */
    std::size_t get_indent() const;

    /*! \brief Write the indent characters to an output stream
     * \param[in] os the output stream
     */
    void write_idents(std::ostream &os);

private:
    //!< \cond DOXY_IMPL
    std::string m_filename;                          //!< The filename
    std::shared_ptr<Data> m_data;                    //!< The XML data
    std::ofstream m_ofs;                             //!< The output stream
    std::map<std::string, bool> m_map_full_elements; //!< The map of full elements
    bool m_escape_single_quote = true;               //!< True if single quote are escaped
    bool m_escape_double_quote = true;               //!< True if double quote are escaped
    std::size_t m_indent = 0;                        //!< The number of characters of an indent
    std::string m_indent_str;                        //!< The ident string
    std::size_t m_cur_indent_idx = 0;                //!< The current ident index
    //!< \endcond
};

} // namespace xml

} // namespace esysfile
