/*!
 * \file esysfile/xml/poco/file.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/xml/filebase.h"
#include "esysfile/xml/functorgen.h"

#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <memory>

namespace esysfile::xml::poco
{

class ESYSFILE_API FileImpl;

/*! \class File esysfile/xml/poco/file.h "esysfile/xml/poco/file.h"
 * \brief A XML file implemeted using POCO
 */
class ESYSFILE_API File : public FileBase
{
public:
    //! Constructor
    File(const std::string &filename = "");

    //! Destructor
    ~File() override;

    int write(const std::string &filename = "") override;
    int write(std::ostream &ouput_stream) override;
    int read(const std::string &filename = "") override;
    int read(std::istream &input_stream) override;

    /*! \brief Get the PIML object
     * \return the PIML object
     */
    FileImpl *get_impl();

private:
    //!< \cond DOXY_IMPL
    std::unique_ptr<FileImpl> m_impl; //!< The PIMPL object
    //!< \endcond
};

} // namespace esysfile::xml::poco

namespace esysfile::xml
{
#ifdef ESYSFILE_XML_USE_POCO
using namespace poco;
#endif

} // namespace esysfile::xml
