/*!
 * \file esysfile/ini/file.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/ini/comment.h"
#include "esysfile/ini/element.h"
#include "esysfile/ini/keyvalue.h"
#include "esysfile/ini/section.h"

#include <fstream>
#include <map>
#include <memory>
#include <vector>

namespace esysfile::ini
{

/*! \class File esysfile/ini/file.h "esysfile/ini/file.h"
 * \brief
 */
class ESYSFILE_API File
{
public:
    //! Constructor
    File();

    File(const std::string &file_path);

    //! Destructor
    virtual ~File();

    void set_file_path(const std::string &file_path);
    const std::string &get_file_path() const;

    int read(const std::string &file_path = "");

    std::vector<std::shared_ptr<Element>> &get_elements();
    const std::vector<std::shared_ptr<Element>> &get_elements() const;

    std::vector<std::shared_ptr<Section>> &get_sections();
    const std::vector<std::shared_ptr<Section>> &get_sections() const;

    std::map<std::string, std::shared_ptr<Section>> &get_sections_map();
    const std::map<std::string, std::shared_ptr<Section>> &get_sections_map() const;

    std::shared_ptr<Section> find_section(const std::string &name) const;

    //! Equal to comparison operator
    bool operator==(const File &other) const;

    //! Not equal to comparison operator
    bool operator!=(const File &other) const;

protected:
    //!< \cond DOXY_IMPL
    virtual bool equal(const File &other) const;
    bool fetch_line();
    const std::string &get_line() const;
    int process_line();
    int process_line_el_type_not_set();
    int process_line_el_type_section();
    int process_line_el_type_comment();
    int process_line_key_value(const std::string &line, std::shared_ptr<KeyValue> key_value);
    void set_cur_el_type(Element::Type cur_el_type);
    Element::Type get_cur_el_type() const;
    Element::Type get_line_el_type() const;
    void set_cur_comment(std::shared_ptr<Comment> cur_comment);
    std::shared_ptr<Comment> get_cur_comment() const;

    void push_back_cur_section(std::shared_ptr<Section> section);
    std::shared_ptr<Section> get_cur_section() const;
    void pop_cur_section();

    int get_section_name_from_line(std::string &section_name, bool &is_sub) const;
    int get_section_name_from_line(const std::string &line, std::string &section_name, bool &is_sub) const;

    std::string m_file_path;
    std::vector<std::shared_ptr<Element>> m_elements;
    std::vector<std::shared_ptr<Section>> m_sections;
    std::map<std::string, std::shared_ptr<Section>> m_sections_map;
    std::ifstream m_ifs;
    std::string m_line;
    int m_line_number = 0;
    Element::Type m_cur_el_type = Element::Type::NOT_SET;
    std::shared_ptr<Comment> m_cur_comment;
    std::vector<std::shared_ptr<Section>> m_cur_sections;
    //!< \endcond
};

} // namespace esysfile::ini
