/*!
 * \file esysfile/ini/section.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/ini/comment.h"
#include "esysfile/ini/element.h"
#include "esysfile/ini/keyvalue.h"

#include <map>
#include <memory>
#include <vector>

namespace esysfile::ini
{

/*! \class Section esysfile/ini/section.h "esysfile/ini/section.h"
 * \brief
 */
class ESYSFILE_API Section : public Element
{
public:
    //! Constructor
    Section();

    //! Destructor
    ~Section() override;

    void add(std::shared_ptr<Comment> comment);
    void add(std::shared_ptr<KeyValue> key_value);
    void add(std::shared_ptr<Section> section);

    std::vector<std::shared_ptr<Element>> &get_elements();
    const std::vector<std::shared_ptr<Element>> &get_elements() const;

    std::vector<std::shared_ptr<KeyValue>> &get_key_values();
    const std::vector<std::shared_ptr<KeyValue>> &get_key_values() const;

    std::map<std::string, std::shared_ptr<KeyValue>> &get_key_values_map();
    const std::map<std::string, std::shared_ptr<KeyValue>> &get_key_values_map() const;

    std::vector<std::shared_ptr<Section>> &get_sections();
    const std::vector<std::shared_ptr<Section>> &get_sections() const;

    std::map<std::string, std::shared_ptr<Section>> &get_sections_map();
    const std::map<std::string, std::shared_ptr<Section>> &get_sections_map() const;

protected:
    //!< \cond DOXY_IMPL
    bool equal(const Element &other) const override;

private:
    std::vector<std::shared_ptr<Element>> m_elements;
    std::vector<std::shared_ptr<KeyValue>> m_key_values;
    std::map<std::string, std::shared_ptr<KeyValue>> m_key_values_map;
    std::vector<std::shared_ptr<Section>> m_sections;
    std::map<std::string, std::shared_ptr<Section>> m_sections_map;
    //!< \endcond
};

} // namespace esysfile::ini
