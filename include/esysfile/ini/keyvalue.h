/*!
 * \file esysfile/ini/keyvalue.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/ini/element.h"

namespace esysfile::ini
{

/*! \class KeyValue esysfile/ini/keyvalue.h "esysfile/ini/keyvalue.h"
 * \brief
 */
class ESYSFILE_API KeyValue : public Element
{
public:
    //! Constructor
    KeyValue();

    //! Destructor
    ~KeyValue() override;

    void set_key_only(bool key_only);
    bool get_key_only() const;

    void set_value(const std::string &value);
    const std::string &get_value() const;

    void set_comment(const std::string &comment);
    const std::string &get_comment() const;

protected:
    //!< \cond DOXY_IMPL
    bool equal(const Element &other) const override;

    std::string m_value;
    std::string m_comment;
    bool m_key_only = false;
    //!< \endcond
};

} // namespace esysfile::ini
