/*!
 * \file esysfile/ini/element.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"

#include <string>

namespace esysfile::ini
{

/*! \class Element esysfile/ini/element.h "esysfile/ini/element.h"
 * \brief
 */
class ESYSFILE_API Element
{
public:
    enum class Type
    {
        NOT_SET,
        COMMENT,
        SECTION,
        KEYVALUE,
        SUBSECTION
    };

    //! Constructor
    Element();

    Element(Type type);

    //! Destructor
    virtual ~Element();

    /*! \brief Set the type of the element
     * \param[in] type the type of the element
     */
    void set_type(Type type);

    /*! \brief Get the type of the element
     * \returnthe type of the element
     */
    Type get_type() const;

    /*! \brief Get the type of the element
     * \returnthe type of the element
     */
    Type &get_type();

    void set_name(const std::string &name);
    const std::string &get_name() const;
    std::string &get_name();

    //! Equal to comparison operator
    bool operator==(const Element &other) const;

    //! Not equal to comparison operator
    bool operator!=(const Element &other) const;

protected:
    //!< \cond DOXY_IMPL
    virtual bool equal(const Element &other) const = 0;

    Type m_type; //!< The type
    std::string m_name;
    //!< \endcond
};

} // namespace esysfile::ini
