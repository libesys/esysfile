/*!
 * \file esysfile/ini/comment.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esysfile/esysfile_defs.h"
#include "esysfile/ini/element.h"

namespace esysfile::ini
{

/*! \class Comment esysfile/ini/comment.h "esysfile/ini/comment.h"
 * \brief
 */
class ESYSFILE_API Comment : public Element
{
public:
    //! Constructor
    Comment();

    //! Destructor
    ~Comment() override;

    void set_text(const std::string &text);
    const std::string &get_text() const;

    void add_line(const std::string &line);

protected:
    //!< \cond DOXY_IMPL
    bool equal(const Element &other) const override;

private:
    std::string m_text;
    //!< \endcond
};

} // namespace esysfile::ini
