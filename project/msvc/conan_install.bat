REM @echo off

set CONAN_FILE=%~dp0..\..\conanfile.py

cd %~dp0..\%1
conan graph info %CONAN_FILE% --format=html --build missing > dependencies.html

IF "%1"=="msvc140" (
  set CONAN_COMP_VER=190
) ELSE IF "%1"=="msvc141" (
  set CONAN_COMP_VER=191
) ELSE IF "%1"=="msvc142" (
  set CONAN_COMP_VER=192
) ELSE IF "%1"=="msvc143" (
  set CONAN_COMP_VER=193
) ELSE (
  exit 1
  echo "ERROR : %1 is not supported"
)

if not exist "%~dp0..\%1\conan_debug_x64\" mkdir %~dp0..\%1\conan_debug_x64
cd %~dp0..\%1\conan_debug_x64
conan install %CONAN_FILE% -s compiler="msvc" -s compiler.version=%CONAN_COMP_VER% -s arch=x86_64 -s build_type=Debug -s compiler.cppstd=17 --build missing --output-folder %~dp0..\%1\conan_debug_x64

if not exist "%~dp0..\%1\conan_release_x64\" mkdir %~dp0..\%1\conan_release_x64
cd %~dp0..\%1\conan_release_x64
conan install %CONAN_FILE% -s compiler="msvc" -s compiler.version=%CONAN_COMP_VER% -s arch=x86_64 -s build_type=Release -s compiler.cppstd=17 --build missing --output-folder %~dp0..\%1\conan_release_x64
