from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, CMakeDeps
from conan.tools.microsoft import MSBuildDeps

import os


class ESysFileConan(ConanFile):
    name = "ESysFile"
    version = "0.1.0"
    license = "wxWindows Library Licence, Version 3.1"
    author = "Michel Gillet michel.gillet@libesys.org"
    url = "https://gitlab.com/libesys/esysfile"
    description = "C++ wrapper for file format, XML"
    topics = ("c++", "xml", "signed xml")
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "poco_use_conan": [True, False],
    }
    # generators = "CMakeToolchain", "MSBuildDeps"
    default_options = {
        "shared": True,
        "fPIC": True,
        "poco_use_conan": False,
        "libxml2/*:shared": True,
        "xmlsec/*:shared": True,
    }

    exports_sources = [
        "include/*",
        "project/*",
        "src/*",
        "res/*",
        "scripts/*",
        "test_packege/*",
        "CMakeLists.txt",
    ]

    def get_lib_version(self, lib, dft_ver):
        env_var_name = "ESYSEXT_%s_VER" % lib.upper()
        version = os.environ.get(env_var_name, dft_ver)
        lib_string = "%s/%s" % (lib, version)
        return lib_string

    def requirements(self):
        self.requires(self.get_lib_version("libiconv", "1.17"), override=True)
        if self.settings.os != "Windows":
            self.requires(self.get_lib_version("boost", "1.71.0"))
            self.requires(self.get_lib_version("poco", "1.11.1"))
            self.requires(self.get_lib_version("libxml2", "2.10.4"))
            self.requires(self.get_lib_version("openssl", "1.1.1t"))
            self.requires(self.get_lib_version("xmlsec", "1.2.32"))
            self.requires(
                self.get_lib_version("ESysTest", "0.1.0@libesys+esystest/test")
            )
        else:
            self.requires(self.get_lib_version("libxml2", "[>=2.11.4 <3.0]"), force=True)
            self.requires(self.get_lib_version("xmlsec", "1.2.30"))
            self.requires(self.get_lib_version("openssl", "3.1.0"))            
            if self.options.poco_use_conan:
                self.requires(self.get_lib_version("poco", "1.12.4"))

    def config_options(self):
        poco_use_conan = False
        if self.settings.os == "Windows":
            del self.options.fPIC
            if self.options["poco_use_conan"]:
                poco_use_conan = True
        else:
            poco_use_conan = True
            self.options["boost"].shared = True
            self.options["poco"].shared = True
            self.options["ESysTest"].shared = True
        if poco_use_conan:
            print("ESysFile: poco use conan")
            self.options["poco"].shared = True
            self.options["poco"].enable_activerecord = False
            self.options["poco"].enable_data = False
            self.options["poco"].enable_data_mysql = False
            self.options["poco"].enable_data_sqlite = False
            self.options["poco"].enable_crypto = False
            self.options["poco"].enable_encodings = False
            self.options["poco"].enable_json = False
            self.options["poco"].enable_jwt = False
            self.options["poco"].enable_mongodb = False
            self.options["poco"].enable_net = False
            self.options["poco"].enable_netssl = False
            self.options["poco"].enable_netssl_win = False
            self.options["poco"].enable_data_postgresql = False
            self.options["poco"].enable_redis = False
            self.options["poco"].enable_util = False
            self.options["poco"].enable_zip = False
        else:
            print("ESysFile: poco doesn't use conan")

    def generate(self):
        if self.settings.os == "Windows":
            ms = MSBuildDeps(self)
            # We assume that -o *:shared=True is used to install all shared deps too
            if self.options.shared:
                ms.configuration = str(self.settings.build_type) + "DLL"
            ms.generate()

    def build(self):
        if self.settings.os != "Windows":
            cmake = CMake(self)
            cmake.definitions["ESYSTEST_BUILD_VHW"] = 1
            cmake.definitions["ESYSTEST_MULTIOS"] = 1
            # cmake.definitions["ESYSTEST_USE_ESYSTEST 1)
            cmake.definitions["ESYSTEST_USE_BOOST"] = 1
            cmake.definitions["ESYSFILE_USE_CONAN"] = "On"
            cmake.configure()
            cmake.build()
            cmake.install()

    def package(self):
        self.copy("*.h", dst="include/esysfile", src="include/esysfile")
        self.copy("*esysfile.lib", dst="lib", keep_path=False)
        # self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*libesysfile.so*", dst="lib", keep_path=False)
        # self.copy("project/cmake/*.cmake", dst="lib/cmake", keep_path=False)
        self.copy("esysfile*.cmake", dst="lib/cmake", keep_path=False)
        # self.copy("*.dylib", dst="lib", keep_path=False)
        # self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["esysfile"]
